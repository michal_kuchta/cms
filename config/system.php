<?php

return [
    'options' => [
        'admin-prefix' => 'admin',
        'frontend' => true,
        'backend' => true,
        'backend-theme' => 'admin',
        'default-theme' => 'default',
        'homeController' => '\App\Http\Controllers\HomeController',
        'functionalities' => [
            'Auth' => true,
            'Administration' => true,
            'Dashboard' => true,
            'Structure' => true,
            'Modules' => true,
            'Configuration' => true
        ]
    ],

    'modules' => [
        'content' => [
            'name' => 'Strona opisowa',
            'controller' => 'App\System\Modules\Cms\ContentController'
        ],
        'articles' => [
            'name' => 'Artykuły',
            'controller' => 'App\System\Modules\Cms\ArticlesController'
        ]
    ],

    'widgets' => [
        'content' => [
            'name' => 'Blok opisowy',
            'controller' => 'App\System\Widgets\Cms\ContentController',
            'view' => 'widgets.cms.content.content'
        ],
        'banner' => [
            'name' => 'Blok bannerowy',
            'controller' => 'App\System\Widgets\Cms\BannerController',
            'view' => 'widgets.cms.banner.content'
        ],
        'menu' => [
            'name' => 'Blok menu',
            'controller' => 'App\System\Widgets\Cms\MenuController',
            'view' => 'widgets.cms.menu.content'
        ],
        'contentList' => [
            'name' => 'Blok listy stron opisowych',
            'controller' => 'App\System\Widgets\Cms\ContentListController',
            'view' => 'widgets.cms.contentList.content'
        ],
        'articleList' => [
            'name' => 'Blok listy artykułów',
            'controller' => 'App\System\Widgets\Cms\ArticleListController',
            'view' => 'widgets.cms.articleList.content'
        ],
    ],

    'sections' => [
        'default' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
        'test' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    ]
];
