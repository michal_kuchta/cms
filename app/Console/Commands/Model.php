<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class Model extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:model {name} {--migration}';

    protected $dirName = 'Models';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create model in new directory.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dirPath = app_path($this->dirName);
        if (!file_exists($dirPath))
        {
            mkdir($dirPath, 0777, true);
        }

        $modelName = $this->dirName . '/' . $this->argument('name');
        $migration = $this->option('migration');
        $this->call('make:model', ['name' => $modelName, '--migration' => $migration]);
    }
}
