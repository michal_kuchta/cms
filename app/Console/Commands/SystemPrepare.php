<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Support\Composer;
use App\Extensions\Database\MigrationCreator;
use Illuminate\Database\Console\Migrations\BaseCommand;

class SystemPrepare extends BaseCommand
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'system:prepare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prepares cms to work';


    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('view:clear');
        $this->call('cache:clear');
        $this->call('config:clear');
        $this->call('route:clear');
        $this->call('migrate');
        $this->call('db:seed');
    }
}
