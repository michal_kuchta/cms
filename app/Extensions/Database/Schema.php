<?php
namespace App\Extensions\Database;

use Illuminate\Support\Facades\DB;
use App\Extensions\Database\Blueprint;

class Schema
{
    /**
     * @var mixed
     */
    public $s = null;
    /**
     * @return mixed
     */
    public function __construct()
    {
        $this->s = DB::connection()->getSchemaBuilder();

        $this->s->blueprintResolver(function ($table, $callback)
        {
            return new Blueprint($table, $callback);
        });
    }
}
