<?php

namespace App\Extensions\Database;

use Illuminate\Database\Schema\Blueprint as BaseBlueprint;

class Blueprint extends BaseBlueprint
{
    public function auditables()
    {
        $this->timestamp('created_at')->nullable();
        $this->timestamp('updated_at')->nullable();
        $this->timestamp('deleted_at')->nullable();
        $this->addColumn('integer', 'created_by')->nullable();
        $this->addColumn('integer', 'updated_by')->nullable();
        $this->addColumn('integer', 'deleted_by')->nullable();
    }
}
