<?php
namespace App\Extensions\Grid\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class FilterNameExistsException extends HttpException
{

}
