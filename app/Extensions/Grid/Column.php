<?php

namespace App\Extensions\Grid;

class Column
{
    public $name;
    public $title;
    public $action;
    public $params;
    public $relationName;
    public $callback;

    public function prepareActionLink($row){
        $url = url()->current(). '/' . $this->action;
        if(!empty($this->params))
        {
            foreach($this->params as $param)
            {
                if(isset($row->$param))
                {
                    $url .= '/' . $row->$param;
                }
            }
        }
        return url($url);
    }
}
