<?php

namespace App\Extensions\Grid;

use App\Extensions\Pager\Pager;
use Exceptions\RowActionLinkExistsException;
use App\Extensions\Grid\Exceptions\FilterNameExistsException;
use App\Extensions\Grid\Exceptions\FilterTypeNotFoundException;
use App\Extensions\Grid\Exceptions\SearchFilterExistsException;

class Grid
{
    private $columns = null;
    private $rows = null;
    private $footerActions = null;
    private $rowsActionLinks = null;
    private $pager = null;
    private $filters = null;
    private $filterTypes = ['checkbox', 'text', 'search', 'select'];
    private $prefix = null;
    private $sessionKey = null;

    public function __construct($sessionKey = null)
    {
        $this->cols = collect();
        $this->rows = collect();
        $this->footerActions = collect();
        $this->rowsActionLinks = collect();
        $this->filters = collect();
        $this->sessionKey = $sessionKey;
    }

    public function setSessionKey($key)
    {
        $this->sessionKey = $key;
    }

    public function getFilters()
    {
        return \Session::get($this->sessionKey.".filters", []);
    }

    /**
     * @param $name
     * @param $title
     * @param $action
     * @param array $params
     * @param string $relationName
     */
    public function addCol($name, $title, $action = '', $params = [], $relationName = '', $callback = null)
    {
        $column = new Column();
        $column->name = $name;
        $column->title = $title;
        $column->action = $action;
        $column->params = $params;
        $column->relationName = $relationName;
        $column->callback = $callback;
        $this->cols->push($column);
    }

    /**
     * @param $rows
     */
    public function addRows($rows)
    {
        foreach($rows as $row)
        {
            $this->addRow($row);
        }
    }

    /**
     * @param $row
     */
    public function addRow($row)
    {
        $this->rows->push($row);
    }

    /**
     * @param string $url
     * @param null $title
     * @param null $icon
     * @param null $class
     */
    public function addRowsActionLink($name, $url, $params = [], $title, $icon = null, $class = null)
    {
        if ($this->rowsActionLinks->has($name))
        {
            throw new RowActionLinkExistsException(401, __('Link akcji o takiej nazwie już istnieje'));
        }

        $action = new \stdClass();
        $action->name = $name;
        $action->url = $url;
        $action->params = $params;
        $action->title = $title;
        $action->icon = $icon;
        $action->class = $class;

        $this->rowsActionLinks->put($name, $action);
    }

    /**
     * @param $name
     * @param $type
     * @param $title
     * @param $url
     * @param array $params
     */
    public function addFooterAction($name, $type, $title, $url = '', $params = [])
    {
        $action = new \stdClass();
        $action->name = $name;
        $action->type = $type;
        $action->title = $title;
        $action->url = $url;
        $action->params = $params;
        $this->footerActions->push($action);
    }

    public function setPager(Pager $pager)
    {
        $this->pager = $pager;
    }

    public function addFilter($name, $type, $label = null, $placeholder = null,  $values = null, $default = null, $colClass = 'col-2')
    {
        if ($this->filters->where('name', '=', "filters[{$name}]")->count() > 0)
        {
            throw new FilterNameExistsException();
        }

        if (!in_array($type, $this->filterTypes))
        {
            throw new FilterTypeNotFoundException();
        }

        if ($type == 'search' && $this->filters->where('type', '=', 'search')->count() > 0)
        {
            throw new SearchFilterExistsException();
        }

        $filter = new \stdClass();
        $filter->name = "filters[{$name}]";
        $filter->label = $label;
        $filter->type = $type;
        $filter->values = $values;
        $filter->default = !empty($default) ? $default : \Session::get($this->sessionKey . '.filters.'.$name, null);
        $filter->placeholder = $placeholder;
        $filter->colClass = $colClass;

        $this->filters->push($filter);
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->rows->count();
    }

    private function prepareRowsActionLinks()
    {
        if($this->rows->count() > 0)
        {
            foreach ($this->rows as &$row)
            {
                $row->actionLinks = collect();
                if ($this->rowsActionLinks->count() > 0)
                {
                    foreach ($this->rowsActionLinks as $actionLink)
                    {
                        $url = $actionLink->url;

                        if (!empty($actionLink->params) && is_array($actionLink->params))
                        {
                            foreach ($actionLink->params as $param)
                            {
                                if (isset($row->$param))
                                {
                                    $url .= '/' . $row->$param;
                                }
                            }
                        }

                        $row->actionLinks->put($actionLink->name, url($url));
                    }
                }
            }
        }
    }

    private function getRenderData()
    {
        $results = [];

        $results = array_merge($results, $this->getRenderFiltersData());
        $results = array_merge($results, $this->getRenderHeaderData());
        $results = array_merge($results, $this->getRenderRowsData());
        $results = array_merge($results, $this->getRenderFooterData());

        return $results;
    }

    private function getRenderFiltersData()
    {
        return [
            'filters' => $this->filters
        ];
    }

    private function getRenderHeaderData()
    {
        return [
            'columns' => $this->cols,
            'showActionsCol' => $this->rowsActionLinks->count() > 0
        ];
    }

    public function onRowData($function)
    {
        foreach($this->rows as &$row)
        {
            $function($row);
        }
    }

    private function getRenderRowsData()
    {
        return [
            'columns' => $this->cols,
            'rows' => $this->rows,
            'rowsActionLinks' => $this->rowsActionLinks
        ];
    }

    private function getRenderFooterData()
    {
        return [
            'actions' => $this->footerActions,
            'pager' => $this->pager
        ];
    }

    public function render()
    {
        $this->prepareRowsActionLinks();
        return \View::make('partials.grid.container', $this->getRenderData())->render();
    }

    public function renderFilters()
    {
        return \View::make('partials.grid.filters', $this->getRenderFiltersData())->render();
    }

    public function renderHeader()
    {
        return \View::make('partials.grid.header', $this->getRenderHeaderData())->render();
    }

    public function renderRows()
    {
        return \View::make('partials.grid.content', $this->getRenderRowsData())->render();
    }

    public function renderFooter()
    {
        return \View::make('partials.grid.footer', $this->getRenderFooterData())->render();
    }
}
