<?php namespace App\Extensions\FluentForm;

use inkvizytor\FluentForm\Base\Handler;
use inkvizytor\FluentForm\Controls\Input;

/**
 * Class TimeZones
 *
 * Example of custom control. Control registered in config file in section 'controls'.
 *
 * @package inkvizytor\FluentForm
 */
class FileManager extends Input
{
    protected $items;

    public function __construct(Handler $handler, $name, $items = [])
    {
        $this->type = 'file';
        $this->items($items);
        $this->name($name);
        parent::__construct($handler);
    }

    /**
     * @param string $type
     * @return $this
     */
    public function type($type)
    {
        // Do nothing

        return $this;
    }

    public function items($items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return string
     */
    public function render()
    {
        $content = parent::render();
        $value = $this->binder()->value($this->key($this->name), $this->value);

        $content = '
            <div class="input-group">
                <span class="input-group-btn">
                    <a id="'.$this->name.'_btn" data-input="'.$this->name.'" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> '.__('Wybierz').'
                    </a>
                </span>
                <div id="filemanager_filesBox_'.$this->name.'" style="width: 100%">';

        $content .= '
                    <div id="filemanager_holderTemplate" class="filemanager_holder" data-counter="0" style="display:none;float:left;padding: 10px;margin: 5px;border: 1px solid black;background: #cccbcb;border-radius: 5px;">
                        <input id="'.$this->name.'" class="form-control" type="hidden" name="'.$this->name.'">
                        <input id="'.$this->name.'_input" class="form-control" readonly="readonly" type="text" name="'.$this->name.'_input">
                        <button class="deletePhoto btn btn-primary">'.__('Usuń').'</button>
                        <div id="'.$this->name.'_holder" style="margin-top:15px;max-height:100px;"></div>
                    </div>';

        if(!empty($this->items))
        {
            foreach($this->items as $id => $item)
            {
                $content .= '
                    <div id="filemanager_holderTemplate" class="filemanager_holder" data-counter="'.($id+1).'" style="float:left;padding: 10px;margin: 5px;border: 1px solid black;background: #cccbcb;border-radius: 5px;">
                        <input id="'.$this->name.'_'.($id+1).'" class="form-control" type="hidden" name="'.$this->name.'[]" value="'.$item->getFileUrl().'">
                        <input id="'.$this->name.'_input_'.($id+1).'" class="form-control" readonly="readonly" type="text" name="'.$this->name.'_input" value="'.$item->getFileName().'">
                        <button class="deletePhoto btn btn-primary">'.__('Usuń').'</button>
                        <div id="'.$this->name.'_holder_'.($id+1).'" style="margin-top:15px;max-height:100px;"><img style="height:5rem" src="'.$item->getThumbUrl().'"></div>
                    </div>';
            }
        }

        $content .= '
                </div>
                <div style="clear:both"></div>
            </div>
            <script>$(document).ready(function(){$("#'.$this->name.'_btn").filemanager("image");})</script>';

        return $content;
    }
}
