<?php namespace App\Extensions\Interfaces;

interface IActivatable
{
    public function isActive();
}
