<?php

/**
 * @param $formatted
 * @return bool|int|string
 */
function stringToByte($formatted)
{
    $formatted = trim($formatted);
    $units = ['KB' => 1, 'MB' => 2, 'GB' => 3, 'TB' => 4, 'PB' => 5, 'EB' => 6, 'ZB' => 7, 'YB' => 8];

    if (!ends_with($formatted, array_keys($units)))
    {
        return $formatted;
    }

    $unit = strtoupper(substr($formatted, -2));
    $value = trim(str_replace($unit, '', $formatted));

    if (!is_numeric($value))
    {
        return 0;
    }

    $value = $value * pow(1024, $units[$unit]);
    $value = head(explode('.', $value));

    return $value;
}

if(!function_exists('pagerUrl'))
{
    function pagerUrl($baseUrl, $params)
    {
        if (empty($params))
        {
            return url($baseUrl);
        }

        $params = http_build_query($params);
        $baseUrl .= '?' . $params;
        return url($baseUrl);
    }
}

/**
 * @param array $values
 * @param array $exclude
 * @param string $separator
 * @return string
 */
function hashJoin(array $values, array $exclude = ['0'], $separator = '#')
{
    $values = array_filter($values, function($var) use ($exclude)
    {
        return !in_array($var, $exclude);
    });

    return $separator.implode($separator, $values).$separator;
}

/**
 * @param string $value
 * @param string $separator
 * @return array
 */
function hashSplit($value, $separator = '#')
{
    return array_filter(explode($separator, trim($value, $separator)), function($var)
    {
        return !empty($var);
    });
}
