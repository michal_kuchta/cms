<?php namespace App\Extensions\Helpers;

class File {
    private $fileName;
    private $fileUrl;
    private $fileDir;
    private $filePath;
    private $fileExtension;

    private $thumbName;
    private $thumbUrl;
    private $thumbDir;
    private $thumbPath;
    private $thumbExtension;

    public function __construct($fileUrl = null)
    {
        $this->setFileUrl($fileUrl);
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    public function getFileUrl()
    {
        return $this->fileUrl;
    }

    public function getFileDir()
    {
        return $this->fileDir;
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public function getFileExtension()
    {
        return $this->fileExtension;
    }

    public function getThumbName()
    {
        return $this->thumbName;
    }

    public function getThumbUrl()
    {
        return $this->thumbUrl;
    }

    public function getThumbDir()
    {
        return $this->thumbDir;
    }

    public function getThumbPath()
    {
        return $this->thumbPath;
    }

    public function getThumbExtension()
    {
        return $this->thumbExtension;
    }

    public function setFileUrl($fileUrl = null)
    {
        $this->fileUrl = $fileUrl;
        $this->processFilePath($fileUrl);
    }

    private function processFilePath($filePath = null)
    {
        if(!$filePath)
        {
            return;
        }

        $parts = explode('/', $filePath);

        $this->fileName = $parts[count($parts)-1];
        $this->thumbName = $parts[count($parts)-1];
        unset($parts[count($parts)-1]);

        $this->fileUrl = $filePath;
        $this->fileDir = str_replace(env('APP_URL').'/', '', implode('/', $parts));
        $this->filePath = $this->fileDir . '/' . $this->fileName;

        $this->thumbDir = $this->fileDir . '/thumbs';
        $this->thumbPath = $this->thumbDir . '/' . $this->fileName;
        $this->thumbUrl = env('APP_URL') . '/' . $this->thumbPath;

        $parts = explode('.', $this->fileName);
        $this->fileExtension = $parts[count($parts)-1];

        $parts = explode('.', $this->thumbName);
        $this->thumbExtension = $parts[count($parts)-1];

        unset($parts);
    }
}
