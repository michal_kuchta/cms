<?php
namespace App\Extensions\Observers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Extensions\Interfaces\IAuditable;

class AuditableObserver
{
    public function saving(IAuditable $model)
    {
        $model->timestamps = false;
    }

    public function creating(IAuditable $model)
    {
        $model->created_at = $model->created_at !== null ? $model->created_at : $this->getDate();
        $model->created_by = $model->created_by !== null ? $model->created_by : $this->getUid();

        $model->updated_at = $this->getDate();
        $model->updated_by = $this->getUid();
    }

    public function updating(IAuditable $model)
    {
        $model->updated_at = $this->getDate();
        $model->updated_by = $this->getUid();
    }

    public function deleting(IAuditable $model)
    {
        $model->deleted_at = $this->getDate();
        $model->deleted_by = $this->getUid();
    }

    public function getUid()
    {
        return empty(Auth::id()) ? 0 : Auth::id();
    }

    public function getDate()
    {
        return Carbon::now(config('app.timezone_display'));
    }
}
