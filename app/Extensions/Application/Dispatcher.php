<?php namespace App\Extensions\Application;

use Exception;
use Carbon\Carbon;
use App\Models\Core\Page;
use Illuminate\View\View;
use App\Models\Core\Widget;
use App\Models\Core\Website;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Route;
use App\Models\Core\Configuration;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Illuminate\View\FileViewFinder;
use Illuminate\Support\ViewErrorBag;
use App\Exceptions\NoWebsiteException;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Response as FacadeResponse;
use App\System\Admin\Modules\Controllers\ContentController;
use Symfony\Component\HttpFoundation\Response as BaseResponse;

/**
 * Class Dispatcher
 *
 * @package App\Extensions\Application
 */
class Dispatcher
{
    private static $website_id = 0;
    private static $redirect = null;
    private static $preview = false;
    private static $manage = false;

    const WIDGET_PLACEHOLDER = '[[[!!![%s]!!!]]]';

    /**
     * @param string $path
     * @param string $args
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public static function route($path, $args)
    {
        $path = hashSplit($path, '/');
        $args = explode(',', pathinfo($args, PATHINFO_FILENAME));

        $response = null;

        $website = self::website();

        $configurationGeneral = Configuration::query()->where('website_id', '=', $website->id)->where('type', '=', 'general')->get();
        app()->singleton('config.general', function() use ($configurationGeneral) { return $configurationGeneral; });
        $page = self::getPage($website->id, null, $path);

        \Request::merge($args);

        $response = self::call($website, $page, ['request' => $args], \Request::method());

        self::csrfProtection($response);

        return $response;
    }

    /**
     * @param Website $website
     * @param Page $page
     * @param array $args
     * @param string $method
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \ReflectionException
     */
    private static function call(Website $website, Page $page, array $args = [], $method = 'GET')
    {
        $params = array_except($args, 'request');
        $args = array_get($args, 'request', []);

        /** @var Controller $controllerName */
        $controllerName = HomeController::class;
        if (!empty($page->module_id))
        {
            $modules = $website->getModules();
            if (!array_key_exists($page->module_id, $modules)) abort(404);

            $modules = config('system.modules');
            if (!array_key_exists($page->module_id, $modules)) abort(404);

            $controllerName = $modules[$page->module_id]['controller'];
        }
        elseif ($page->page_type == 1)
        {
            $controllerName = config('system.options.homeController', '\App\Http\Controllers\HomeController');
        }
        else
        {
            abort(404);
        }

        /** @var Request $request */
        $request = \Route::getCurrentRequest();

        \View::share('website_id', $website->id);

        $action = array_shift($args);
        $actionName = empty($action) ? 'index' : $action;
        $actionName = strtolower($method).ucfirst(camel_case($actionName));

        // Sprawdzenie czy akcja istnieje
        $reflection = new \ReflectionClass($controllerName);
        $controller = $reflection->newInstanceWithoutConstructor();
        if (!$reflection->hasMethod($actionName))
        {
            if ($reflection->hasMethod($action))
            {
                $actionName = $action;
            }
            if (!$reflection->hasMethod($actionName))
            {
                array_unshift($args, $action);
                $actionName = "catchAll";
            }
        }
        else if ($reflection->getMethod($actionName)->getName() !== $actionName)
        {
            abort(404);
        }

        $routes = \Route::getRoutes();
        $action = $routes->getByName('frontend');
        $middleware = $action->middleware();
        self::disableRoute($action);

        $action = $routes->getByName('catch');
        self::disableRoute($action);

        $routes = \Route::getRoutes();
        $middleware = ['web', 'frontend'];

        $route = new Route(
            ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
            '{__pattern__?}',
            [
                // tu nie dodajemy middleware bo co za duzo to nie zdrowo
                'uses' => $controllerName.'@'.$actionName,
                'controller' => $controllerName.'@'.$actionName,
                'as' => 'dispatched'
            ]
        );
        $route->where([
            '__pattern__' => '.*'
        ]);
        $route->setRouter(app('router'));
        $route->setContainer(app());
        $route->bind($request);
        $routes->add($route);

        foreach ($route->parameters() as $key => $value)
        {
            $route->forgetParameter($key);
        }

        foreach ($args as $key => $value)
        {
            $route->defaults($key, $value);
        }

        /** @var Response $response */
        $response = \Route::dispatch($request);

        self::renderWidgets($request, $response, $website, $page);

        return $response;
    }

    private static function renderWidgets(Request $request, BaseResponse $response, Website $website, Page $page)
    {
        $content = $response->getContent();
        $widgets = Widget::query()
            ->where('website_id', '=', $website->id)
            ->where('is_active', '=', 1)
            ->get();

        $insertedFields = [];
        foreach($widgets as $widget)
        {
            if((int)$widget->settings['section'] == 0)
                continue;

            $fieldName = sprintf(self::WIDGET_PLACEHOLDER, 'field:'.$widget->settings['section']);
            $widgetClass = config('system.widgets.'.$widget->type.'.controller');
            $widgetComposer = \App::make($widgetClass);

            $widgetView = \View::make(config('system.widgets.'.$widget->type.'.view'))
                ->with('__WEBPART', md5($widget->id));

            \App::call([$widgetComposer, 'compose'], ['view' => $widgetView, 'data' => $widget->settings]);
            $content = str_replace($fieldName, $widgetView->render(), $content);
            $insertedFields[$widget->settings['section']] = $widget->settings['section'];
        }

        $availableFields = config('system.sections.'.$website->symbol);
        foreach($availableFields as $field)
        {
            if(!in_array($field, $insertedFields))
            {
                $fieldName = sprintf(self::WIDGET_PLACEHOLDER, 'field:'.$field);
                $content = str_replace($fieldName, '', $content);
            }
        }

        $response->setContent($content);
    }

    private static function disableRoute($route)
    {
        if ($route != null)
        {
            // Inaczej się nie da wyłączyć trasy bo cacheują wzorzec w $this->compiled
            $reflectionClass = new \ReflectionClass(Route::class);
            $reflectionProperty = $reflectionClass->getProperty('compiled');
            $reflectionProperty->setAccessible(true);
            $reflectionProperty->setValue($route, null);

            $route->setUri('__off__');
        }
    }

    /**
     * @param string $domain
     * @param int|null $website_id
     * @return \Edito\Models\Core\Website $website
     */
    public static function website($website_id = null)
    {
        /** @var Website $website */
        $website = null;

        if (!empty($website_id))
        {
            $website = Website::query()->find($website_id);
        }
        else if (isset($_SERVER['SERVER_NAME']) || isset($_SERVER['HTTP_HOST']))
        {
            $baseUrl = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : $_SERVER['HTTP_HOST'];
            $website = Website::query()->where('base_url', '=', $baseUrl)->first();
        }

        if(!$website)
        {
            $website = Website::query()->first();
        }

        if ($website == null)
        {
            throw new NoWebsiteException(404);
        }
        if ($website->is_active == false)
        {
            abort(404);
        }

        // Ścieżka do katalogu public
        \App::bind('path.public', function() use ($website)
        {
            return base_path().'/public/themes/'.$website->theme;
        });

        // Ścieżka do szablonów
        $finder = \View::getFinder();

        if ($finder instanceof FileViewFinder)
        {
            $finder->addLocation(base_path());
            $finder->addLocation(base_path("resources/system/themes/{$website->theme}"));
        }

        // Zapamiętanie id witryny
        self::$website_id = $website->id;

        app()->singleton('app.website', function() use ($website) { return $website; });
        app()->singleton(Website::class, function() use ($website) { return $website; });
        \View::share('themeName', $website->theme);
        return $website;
    }

    /**
     * @param int $website_id
     * @param string $locale
     * @param int|null $page_id
     * @param array $path
     * @param array $args
     * @return Page
     */
    private static function getPage($website_id, $page_id = null, array &$path = [])
    {
        /** @var Page $page */
        $page = null;

        if (empty($page_id) && !empty($path))
        {
            $page = Page::getByPath($website_id, $path);
        }
        else
        {
            $page = Page::find($page_id);
        }

        if ($page == null)
        {
            $page = Page::where('page_type', '=', 1)->first();
        }

        if ($page == null)
        {
            abort(404);
        }

        return self::preparePage($page);
    }

    private static function preparePage(Page $page)
    {
        if ($page->is_active == false)
        {
            abort(404);
        }

        app()->singleton('app.page', function() use ($page) { return $page; });
        app()->singleton(Page::class, function() use ($page) { return $page; });

        return $page;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Response $response
     */
    private static function csrfProtection(BaseResponse $response)
    {
        $content = $response->getContent();

        // CSRF Token
        $content = preg_replace_callback('/<meta(.*?)name="csrf-token"(.*?)\/>/i', function($matches)
        {
            return preg_replace('/content="(.+?)"/', 'content="'.\Session::token().'"', $matches[0]);
        },
        $content);

        $content = preg_replace_callback('/<input(.*?)name="_token"(.*?)\/>/i', function($matches)
        {
            return preg_replace('/value="(.+?)"/', 'value="'.\Session::token().'"', $matches[0]);
        },
        $content);

        $response->setContent($content);
    }

    public static function executeStorageDispatcher($path = null)
    {
        $filePath = Storage::url($path);
        if ( ! \File::exists($filePath) or ( ! $mimeType = getImageContentType($filePath)))
        {
            dd($filePath);
            return abort(404);
        }

        $fileContents = \File::get($filePath);
        return FacadeResponse::make($fileContents, 200, array('Content-Type' => $mimeType));
    }
}
