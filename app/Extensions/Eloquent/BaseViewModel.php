<?php
namespace App\Extensions\Eloquent;

use ReflectionClass;
use ReflectionProperty;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class BaseViewModel
{
    /** @var array */
    private $publicAttributes = [];

    /** @var array */
    private $protectedAttributes = [];

    /**
     * @return array
     */
    public function getPublicAttributes()
    {
        $this->attributesIdentyfication();

        return $this->publicAttributes;
    }

    /**
     * @return array
     */
    public function getProtectedAttributes()
    {
        $this->attributesIdentyfication();

        return $this->protectedAttributes;
    }

    /**
     * @return array
     */
    public function getAllAttributes()
    {
        $this->attributesIdentyfication();

        return array_merge($this->publicAttributes, $this->protectedAttributes);
    }

    /**
     * @param $name
     * @return bool
     */
    private function isProtected($name)
    {
        $this->attributesIdentyfication();

        return in_array($name, $this->protectedAttributes);
    }

    /**
     * Tworzy listy publicznych i chronionych właściwości
     */
    private function attributesIdentyfication()
    {
        if (count($this->publicAttributes) == 0)
        {
            $reflection = new ReflectionClass($this);

            foreach ($reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $property)
            {
                $this->publicAttributes[] = $property->name;
            }

            foreach ($reflection->getProperties(ReflectionProperty::IS_PROTECTED) as $property)
            {
                $this->protectedAttributes[] = $property->name;
            }
        }
    }

    /**
     * @param array|object $attributes
     * @return $this
     */
    public function fill($attributes = array())
    {
        // Musimy uzupełnić zarówno publiczne jak i chronione właściwości
        foreach ($this->getAllAttributes() as $key)
        {
            $value = null;

            if ($attributes instanceof Model)
            {
                $value = $attributes->getAttribute($key);
            }
            else if (is_array($attributes))
            {
                $value = array_get($attributes, $key);
            }

            if ($value !== null)
            {
                if ($this->{$key} instanceof Collection)
                {
                    $this->{$key} = $this->{$key}->make($value);
                }
                else
                {
                    $this->{$key} = $value;
                }
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getRawValues()
    {
        $array = array();

        foreach ($this->getPublicAttributes() as $attribute)
        {
            $array[$attribute] = $this->{$attribute};
        }

        return $array;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->getRawValues();
    }

    /**
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * @param string $property
     * @return string
     */
    public function __get($property)
    {
        return $this->getValue($property);
    }

    /**
     * @param $property
     * @param $value
     */
    public function __set($property, $value)
    {
        $this->setValue($property, $value);
    }

    /**
     * @param string $name
     * @return mixed
     */
    private function getValue($name)
    {
        if ($this->isProtected($name))
        {
            $class = new ReflectionClass($this);
            $property = $class->getProperty($name);
            $property->setAccessible(true);

            return $property->getValue($this);
        }

        return null;
    }

    /**
     * @param $name
     * @param $value
     */
    private function setValue($name, $value)
    {
        if ($this->isProtected($name))
        {
            $class = new ReflectionClass($this);
            $property = $class->getProperty($name);
            $property->setAccessible(true);
            $property->setValue($this, $value);
        }
    }

}
