<?php namespace App\Extensions\Eloquent;

use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;
use Illuminate\Validation\Validator;
use inkvizytor\FluentForm\Base\IModel;

abstract class ViewModel extends BaseViewModel implements IModel
{
    /** @var Validator */
    private $validator = null;

    /** @var bool */
    private $valid = null;

    /**
     * @return bool
     */
    public function isValid()
    {
        if ($this->valid === null)
        {
            $this->valid = $this->getValidator()->passes();

            $errors = $this->getValidator()->errors();
            $viewbag = \Session::get('errors', new ViewErrorBag);
            $viewbag->put(get_class($this), $errors);

            \Session::flash('errors', $viewbag);
        }

        return $this->valid;
    }

    /**
     * Returns Laravel's validator rules array
     * @return array
     */
    public function getValidatorRules()
    {
        return [];
    }

    /**
     * Returns Laravel's validator messages array
     * @return array
     */
    public function getMessages()
    {
        return [];
    }

    /**
     * Returns Laravel's validator field names array
     * @return array
     */
    public function getFieldNames()
    {
        return [];
    }

    /**
     * Create Laravel's native validator
     * @return Validator
     */
    public function createValidator()
    {
        return \Validator::make($this->getRawValues(), $this->getValidatorRules(), $this->getMessages(), $this->getFieldNames());
    }

    /**
     * Returns Laravel's native validator
     * @return Validator
     */
    public function getValidator()
    {
        if ($this->validator === null)
        {
            $this->validator = $this->createValidator();
        }

        return $this->validator;
    }

    /**
     * Returns laravel validator instance
     * @return MessageBag
     */
    public function getErrors()
    {
        if ($this->valid !== null)
        {
            return $this->getValidator()->errors();
        }

        /** @var ViewErrorBag $viewbag */
        $viewbag = \Session::get('errors', new ViewErrorBag);

        return $viewbag->getBag(get_class($this));
    }

    public function errors()
    {
        return $this->getErrors();
    }

    /**
     * Checks if the given field is required
     * @param $fieldName
     * @return bool
     */
    public function isRequired($fieldName)
    {
        $rules = $this->getValidatorRules();

        if(isset($rules[$fieldName]) && preg_match('/required/', $rules[$fieldName]))
        {
            return true;
        }

        return false;
    }

    public function rules()
    {
        return $this->getValidatorRules();
    }
}
