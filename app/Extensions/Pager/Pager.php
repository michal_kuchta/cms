<?php

namespace App\Extensions\Pager;

class Pager
{
    private $rowCount;
    private $pageSize;
    private $pageIndex;
    private $totalPages;
    private $baseUrl;

    /**
     * @param int $pageSize = 10
     * @param int $rowCount = 0
     * @param int $pageIndex = 1
     * @param int $baseUrl = null
     */
    public function __construct($pageSize = 10, $rowCount = 0, $pageIndex = 1, $baseUrl = null)
    {
        $this->setPageSize($pageSize);
        $this->setRowCount($rowCount);
        $this->setPageIndex($pageIndex);
        $this->setBaseUrl($baseUrl);
    }

    public function setRowCount($rowCount)
    {
        $this->rowCount = $rowCount;
        $this->totalPages = ceil($rowCount / $this->getPageSize());
    }

    public function getRowCount()
    {
        return $this->rowCount;
    }

    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
    }

    public function getPageSize()
    {
        return $this->pageSize;
    }

    public function setPageIndex($pageIndex)
    {
        $this->pageIndex = $pageIndex;
    }

    public function getPageIndex()
    {
        return $this->pageIndex;
    }

    public function getTotalPages()
    {
        return $this->totalPages;
    }

    public function setBaseUrl($url)
    {
        $this->baseUrl = $url;
    }

    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    public function getStartPoint()
    {
        if ($this->getPageIndex() - 4 <= 1)
        {
           return 1;
        }

        return $this->getPageIndex() - 4;
    }

    public function getEndPoint()
    {
        if ($this->getPageIndex() + 4 < $this->getTotalPages())
        {
            return $this->getPageIndex() + 4;
        }

        return $this->getTotalPages();
    }

    public function getOffset()
    {
        return $this->getPageIndex() > 1 ? ( $this->getPageIndex() - 1 ) * $this->getPageSize() : 0;
    }

    public function render()
    {
        return \View::make('partials.pager.pager', ['pager' => $this]);
    }
}
