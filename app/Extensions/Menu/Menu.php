<?php
namespace App\Extensions\Menu;

class Menu
{
    /**
     * @var mixed
     */
    private $menuNodes = null;

    /**
     * @param $sitemap
     */
    public function __construct($sitemap)
    {
        $this->menuNodes = new MenuNodes();
        $this->createMenu($sitemap, $this->menuNodes);
    }

    /**
     * @param array $sitemap
     */
    private function createMenu($sitemap = [], &$menuNodes)
    {
        foreach ($sitemap as $menuItem)
        {
            if ($this->isValidMenuItem($menuItem) && $this->menuNodes instanceof MenuNodes)
            {
                $menuNode = new MenuNode();
                $menuNode->setLevel($menuItem['level']);
                $menuNode->setTitle($menuItem['title']);
                $menuNode->setName($menuItem['name']);
                $menuNode->setUrl($menuItem['url']);
                $menuNode->setVisibility($menuItem['visible']);
                $menuNode->setIcon($menuItem['icon']);

                if (isset($menuItem['nodes']) && is_array($menuItem['nodes']) && !empty($menuItem['nodes']))
                {
                    $this->createMenu($menuItem['nodes'], $menuNode);
                }

                $menuNodes->addNode($menuNode);
            }
        }
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->menuNodes;
    }

    /**
     * @param $menuItem
     */
    private function isValidMenuItem($menuItem)
    {
        return is_array($menuItem) && isset($menuItem['name']) && isset($menuItem['title']) && isset($menuItem['url']) && isset($menuItem['visible']);
    }
}
