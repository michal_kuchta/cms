<?php
namespace App\Extensions\Menu;

class MenuNodes
{
    /**
     * @var array
     */
    private $nodes = null;

    public function __construct()
    {
        $this->nodes = collect();
    }

    /**
     * @return mixed
     */
    public function getNodes()
    {
        return $this->nodes;
    }

    /**
     * @param MenuNode $node
     */
    public function addNode(MenuNode $node)
    {
        $this->nodes->push($node);
    }

    /**
     * @param $id
     * @param $name
     * @param $title
     * @param $url
     */
    public function addNodeData($name, $title, $url, $visibility)
    {
        $node = new MenuNode($name, $title, $url, $visibility);
        $this->nodes->push($node);
    }

    /**
     * @return mixed
     */
    public function hasNodes()
    {
        return $this->nodes->count() > 0;
    }
}
