<?php
namespace App\Extensions\Menu;

class MenuNode
{
    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $visibility = '';

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var mixed
     */
    private $url = '';

    /**
     * @var mixed
     */
    private $nodes = null;

    private $icon = '';

    private $isActive = null;

    private $level = null;

    /**
     * @param $id
     * @param $name
     * @param $title
     * @param $url
     */
    public function __construct($name = '', $title = '', $url = '', $visibility = false, $icon = '', $active = false, $level = 2)
    {
        $this->name = $name;
        $this->title = $title;
        $this->url = $url;
        $this->visibility = $visibility;
        $this->icon = $icon;
        $this->isActive = !empty($url) ? $this->checkActivity($url) : $active;
        $this->level = $level;

        $this->nodes = new MenuNodes();
    }

    public function setActivity($bool = false)
    {
        $this->isActive = $bool;
    }

    public function isActive()
    {
        return $this->isActive;
    }

    public function setLevel($level = 2)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param $url
     */
    public function setUrl($url, $level = 2)
    {
        $this->url = $url;
        $this->isActive = $this->checkActivity($url, $this->level);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function isVisible()
    {
        return $this->visibility;
    }

    /**
     * @param $visibility
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;
    }

    /**
     * @return mixed
     */
    public function hasNodes()
    {
        return $this->nodes->hasNodes();
    }

    /**
     * @return mixed
     */
    public function getNodes()
    {
        return $this->nodes->getNodes();
    }

    /**
     * @param $name
     * @param $title
     * @param $url
     * @param $visibility
     */
    public function addNode(MenuNode $node)
    {
        $this->nodes->addNode($node);
    }

    /**
     * @param $name
     * @param $title
     * @param $url
     * @param $visibility
     */
    public function addNodeData($name, $title, $url, $visibility)
    {
        $this->nodes->addNodeData($name, $title, $url, $visibility);
    }

    public function setIcon($name)
    {
        $this->icon = $name;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    private function checkActivity($url, $level = 2)
    {
        $routeUri = implode('/', array_slice(explode('/', \Route::current()->uri), 0, $level));
        return $url === $routeUri;
    }
}
