<?php
namespace App\System\Widgets\Cms;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Models\System\Content;
use App\Http\Controllers\Controller;

/**
 * Class ContentListController
 *
 * @package App\System\Widgets\Cms
 */
class ContentListController extends Controller
{
    public function compose(View $view, $settings)
    {
        $settings['contentsModels'] = null;
        if(!empty($settings['contents']) && count($settings['contents']) > 0)
        {
            $contentsQuery = Content::query()
                ->whereIn('id', $settings['contents'])
                ->where('website_id', '=', app('app.website')->id)
                ->limit($settings['count']);

            if(isset($settings['is_random']) && (bool)$settings['is_random'])
            {
                $contentsQuery->inRandomOrder();
            }

            $contents = $contentsQuery->get();
            $settings['contentsModels'] = $contents;
        }

        $view->with('settings', $settings);
    }
}
