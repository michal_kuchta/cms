<?php
namespace App\System\Widgets\Cms;

use App\Models\Core\Page;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class MenuController
 *
 * @package App\System\Widgets\Cms
 */
class MenuController extends Controller
{
    public function compose(View $view, $settings)
    {
        $menuItems = Page::getListByGroup($view->settings['group_id']);
        $view->with('menuItems', $menuItems);
        $view->with('settings', $settings);
    }
}
