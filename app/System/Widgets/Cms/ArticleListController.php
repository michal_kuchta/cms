<?php
namespace App\System\Widgets\Cms;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Models\System\Article;
use App\Http\Controllers\Controller;

/**
 * Class ArticleListController
 *
 * @package App\System\Widgets\Cms
 */
class ArticleListController extends Controller
{
    public function compose(View $view, $settings)
    {
        $settings['articlesModels'] = collect();
        if(!empty($settings['pageId']))
        {
            $contentsQuery = Article::query()
                ->where('page_id', '=', $settings['pageId'])
                ->where('website_id', '=', app('app.website')->id)
                ->limit($settings['count']);

            if(isset($settings['is_random']) && (bool)$settings['is_random'])
            {
                $contentsQuery->inRandomOrder();
            }

            $contents = $contentsQuery->get();
            $settings['articlesModels'] = $contents;
        }

        $view->with('settings', $settings);
    }
}
