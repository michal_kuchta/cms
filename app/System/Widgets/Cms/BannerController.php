<?php
namespace App\System\Widgets\Cms;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class BannerController
 *
 * @package App\System\Widgets\Cms
 */
class BannerController extends Controller
{
    public function compose(View $view, $settings)
    {
        $view->with('settings', $settings);
    }
}
