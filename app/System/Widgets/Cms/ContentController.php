<?php
namespace App\System\Widgets\Cms;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ContentController
 *
 * @package App\System\Widgets\Cms
 */
class ContentController extends Controller
{
    public function compose(View $view, $settings)
    {
        $view->with('settings', $settings);
    }
}
