<?php
namespace App\System\Modules\Cms;

use App\Models\Core\Website;
use Illuminate\Http\Request;
use App\Models\System\Content;
use App\Http\Controllers\Controller;

/**
 * Class ContentController
 *
 * @package App\System\Modules\Cms
 */
class ContentController extends Controller
{
    public function getIndex()
    {
        $content = Content::getContentByPageId(app('app.page')->id);
        return view('views.cms.content.index', compact('content'));
    }
}
