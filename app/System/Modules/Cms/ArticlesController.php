<?php
namespace App\System\Modules\Cms;

use App\Models\Core\Website;
use Illuminate\Http\Request;
use App\Models\System\Article;
use App\Http\Controllers\Controller;

/**
 * Class ArticlesController
 *
 * @package App\System\Modules\Cms
 */
class ArticlesController extends Controller
{
    public function getIndex()
    {
        $articles = Article::getArticles(app('app.page')->id);
        $pageTitle = app('app.page')->title;
        return view('views.cms.articles.index', compact('articles', 'pageTitle'));
    }

    public function catchALl($slug = null)
    {
        if (!$slug)
        {
            abort(404);
        }

        $slugs = explode('/', $slug);

        $symbol = last($slugs);

        $article = Article::getArticle(app('app.page')->id, $symbol);

        if (!$article)
        {
            abort(404);
        }

        return view('views.cms.articles.detail', compact('article'));
    }
}
