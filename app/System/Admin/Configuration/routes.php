<?php

Route::group(['prefix' => config('system.options.admin-prefix')], function()
{
    Route::group(['prefix' => 'configuration', 'namespace' => 'App\System\Admin\Configuration\Controllers', 'middleware' => ['web', 'auth', 'backend']], function ()
    {
        Route::controller('general', 'GeneralController');
    });
});
