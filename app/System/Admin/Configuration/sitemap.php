<?php

return [
    ['name' => 'configuration', 'title' => __('Konfiguracja'), 'url' => 'admin/configuration', 'visible' => config('system.options.functionalities.Configuration'), 'icon' => 'icon-th-list', 'level' => 2, 'nodes' => [
        ['name' => 'general', 'title' => __('Ogólna'), 'url' => 'admin/configuration/general', 'visible' => true, 'icon' => 'icon-file-text-o', 'level' => 3],
    ]]
];
