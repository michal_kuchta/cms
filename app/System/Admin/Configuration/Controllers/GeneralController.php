<?php
namespace App\System\Admin\Configuration\Controllers;

use App\Models\Core\Website;
use Illuminate\Http\Request;
use App\Models\Core\Configuration;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\ViewModels\Core\Configuration\CreateViewModel;

/**
 * Class GeneralController
 *
 * @package App\System\Admin\Structure\Controllers
 */
class GeneralController extends Controller
{
 
    public function getIndex(Request $request)
    {
        $entity = Configuration::query()
        ->with(['website'])
        ->where('website_id', '=', app('app.website')->id)
        ->where('type', '=', 'general')
        ->first();

        return view('views.configuration.general.index', compact('entity'));
    }

    public function postIndex(Request $request)
    {
        $entity = Configuration::query()->where('website_id', '=', app('app.website')->id)->where('type', '=', 'general')->first();
        if($entity)
        {
            $entity->delete();
        }
        $input = $request->all();
        $model = new CreateViewModel($input);
        $entity = new Configuration();
        $entity->fill($model->toArray());
        $entity->website_id = app('app.website')->id;
        $entity->type = 'general';
        $entity->save();

        \Flash::success(__('Konfiguracja została zapisana.'));
        return \Redirect::to(url('admin/configuration/general'));
    }
}
