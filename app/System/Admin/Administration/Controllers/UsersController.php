<?php
namespace App\System\Admin\Administration\Controllers;

use App\Models\Core\Page;
use App\Models\Core\User;
use App\Models\Core\Website;
use Illuminate\Http\Request;
use App\Extensions\Grid\Grid;
use App\Models\System\Article;
use App\Extensions\Pager\Pager;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\ViewModels\Core\Users\EditViewModel;
use App\ViewModels\Core\Users\CreateViewModel;

/**
 * Class UsersController
 *
 * @package App\System\Admin\Structure\Controllers
 */
class UsersController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getIndex()
    {
        $pageId = Input::get('pageId', 1);
        $grid = new Grid();

        $grid->addCol('id', __('ID'));
        $grid->addCol('rowName', __('Nazwa'), 'edit', ['id']);

        $pager = new Pager();
        $pager->setPageIndex($pageId);

        $query = User::query()->where('website_id', '=', app('app.website')->id);

        $pager->setRowCount($query->count());
        $pager->setBaseUrl('admin/administration/users');

        $rows = $query
            ->skip($pager->getOffset())
            ->take($pager->getPageSize())
            ->get();

        $grid->setPager($pager);
        $grid->addRows($rows);

        $grid->onRowData(function($row){
            $row->rowName = $row->first_name . ' ' . $row->last_name;
        });

        $grid->addRowsActionLink('edit', url('admin/administration/users/edit'), ['id'], __('Edytuj'));
        $grid->addRowsActionLink('delete', url('admin/administration/users/delete'), ['id'], __('Usuń'));

        $grid->addFooterAction('add', 'link', __('Dodaj'), 'admin/administration/users/create');

        return view('views.administration.users.index', ['grid' => $grid]);
    }

    public function getCreate()
    {
        $model = new CreateViewModel();
        $website_id = app('app.website')->id;
        return view('views.administration.users.create', compact('model', 'website_id'));
    }

    public function postCreate(Request $request)
    {
        $model = new CreateViewModel($request->all());
        if($model->isValid())
        {
            $model->is_admin = empty($model->is_admin) ? 0 : $model->is_admin;
            $entity = new User();
            $entity->fill($model->toArray());
            $entity->password = \Hash::make($model->password);
            $entity->save();

            \Flash::success(__('Wpis został dodany.'));
            return \Redirect::to(url('admin/administration/users/edit', [$entity->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getEdit($id)
    {
        $entity = User::query()
        ->where('id', '=', $id)
        ->where('website_id', '=', app('app.website')->id)
        ->first();

        if(!$entity)
        {
            \Flash::success(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('userscontroller.getindex');
        }

        $model = new EditViewModel($entity);
        $model->fill(Input::old());

        $pages = Page::getPagesForModule('articles');

        return view('views.administration.users.edit', compact('model', 'entity'));
    }

    public function postEdit(Request $request, $id)
    {
        $model = new EditViewModel($request->all());
        if($model->isValid())
        {
            $model->is_admin = 0;
            if($request->get('is_admin'))
            {
                $model->is_admin = 1;
            }

            $modelArray = $model->toArray();
            unset($modelArray['new_password']);

            $entity = User::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();
            $entity->fill($modelArray);

            if(!empty($model->new_password))
            {
                $entity->password = \Hash::make($model->new_password);
                \Flash::success(__('Hasło zostało zmienione pomyślnie.'));
            }

            $entity->save();

            \Flash::success(__('Wpis został zapisany.'));
            return \Redirect::to(url('admin/administration/users/edit', [$entity->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getDelete(Request $request, $id)
    {
        $entity = User::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('userscontroller.getindex');
        }

        return view('views.administration.users.delete', compact('entity'));
    }

    public function postDelete($id)
    {
        $entity = User::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->delete();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('userscontroller.getindex');
        }

        \Flash::success(__('Użytkownik został usunięty.'));
        return \Redirect::route('userscontroller.getindex');
    }
}
