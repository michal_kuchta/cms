<?php

return [
    ['name' => 'administration', 'title' => __('Zarządznie'), 'url' => 'admin/administration', 'visible' => config('system.options.functionalities.Administration'), 'icon' => 'icon-cog-alt', 'level' => 2, 'nodes' => [
        ['name' => 'users', 'title' => __('Użytkownicy'), 'url' => 'admin/administration/users', 'visible' => true, 'icon' => 'icon-users', 'level' => 3],
    ]]
];
