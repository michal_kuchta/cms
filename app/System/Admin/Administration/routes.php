<?php

Route::group(['prefix' => config('system.options.admin-prefix')], function()
{
    Route::group(['prefix' => 'administration', 'namespace' => 'App\System\Admin\Administration\Controllers', 'middleware' => ['web', 'auth', 'backend']], function ()
    {
        Route::controller('users', 'UsersController');
    });
});
