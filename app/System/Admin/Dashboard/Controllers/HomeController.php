<?php
namespace App\System\Admin\Dashboard\Controllers;

use App\Http\Controllers\Controller;

/**
 * Class HomeController
 *
 * @package App\System\Admin\Dashboard\Controllers
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getIndex()
    {
        return view('views.dashboard.home.index');
    }
}
