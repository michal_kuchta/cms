<?php

Route::group(['prefix' => config('system.options.admin-prefix'), 'namespace' => 'App\System\Admin\Dashboard\Controllers', 'middleware' => ['web', 'auth', 'backend']], function()
{
    Route::get('/', [
        'uses' => 'HomeController@getIndex',
        'as' => 'home'
    ]);
    
 });