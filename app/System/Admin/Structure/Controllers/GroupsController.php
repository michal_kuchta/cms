<?php
namespace App\System\Admin\Structure\Controllers;

use App\Models\Core\Page;
use App\Models\Core\Website;
use Illuminate\Http\Request;
use App\Extensions\Grid\Grid;
use App\Extensions\Pager\Pager;
use App\Models\Core\PagesGroup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\ViewModels\Core\PagesGroups\EditViewModel;
use App\ViewModels\Core\PagesGroups\CreateViewModel;
use App\ViewModels\Core\PagesGroups\DeleteViewModel;

/**
 * Class GroupsController
 *
 * @package App\System\Admin\Structure\Controllers
 */
class GroupsController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getIndex()
    {
        $pageId = Input::get('pageId', 1);
        $grid = new Grid();

        $grid->addCol('id', __('ID'));
        $grid->addCol('name', __('Nazwa'));
        $grid->addCol('created_at', __('Data utworzenia'));
        $grid->addCol('updated_at', __('Data edycji'));

        $query = PagesGroup::query()->where('website_id', '=', app('app.website')->id);

        $pager = new Pager();
        $pager->setPageSize(2);
        $pager->setPageIndex($pageId);

        $pager->setRowCount($query->count());
        $pager->setBaseUrl('admin/structure/groups');

        $data = $query->skip($pager->getOffset())
            ->take($pager->getPageSize())
            ->get();

        $grid->setPager($pager);
        $grid->addRows($data);
        $grid->addRowsActionLink('editWebsite', url('admin/structure/groups/edit'), ['id'], __('Edytuj'));
        $grid->addRowsActionLink('deleteWebsite', url('admin/structure/groups/delete'), ['id'], __('Usuń'));

        $grid->addFooterAction('addPage', 'link', __('Dodaj'), 'admin/structure/groups/create');

        return view('views.structure.groups.index', ['grid' => $grid]);
    }

    public function getCreate()
    {
        $website = app('app.website');

        return view('views.structure.groups.create', compact('website'));
    }

    public function postCreate(Request $request)
    {
        $model = new CreateViewModel($request->all());
        $model->website_id = app('app.website')->id;

        if($model->isValid())
        {
            $page = new PagesGroup();
            $page->fill($model->toArray());
            $page->save();

            \Flash::success(__('Wpis został dodany.'));
            return \Redirect::to(url('admin/structure/groups/edit', [$page->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getEdit($id)
    {
        $entity = PagesGroup::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('groupscontroller.getindex');
        }

        $model = new EditViewModel($entity);
        $model->fill(Input::old());

        return view('views.structure.groups.edit', compact('model', 'entity'));
    }

    public function postEdit(Request $request, $id)
    {
        $model = new EditViewModel($request->all());

        if($model->isValid())
        {
            $entity = PagesGroup::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();

            if(!$entity)
            {
                \Flash::info(__('Brak wpisu o pdanym ID'));
                return \Redirect::route('groupscontroller.getindex');
            }

            $entity->fill($model->toArray());
            $entity->save();

            \Flash::success(__('Wpis został zapisany.'));
            return \Redirect::to(url('admin/structure/groups/edit', [$entity->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getDelete($id)
    {
        $entity = PagesGroup::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('groupscontroller.getindex');
        }

        $model = new DeleteViewModel($entity);
        $model->fill(Input::old());

        return view('views.structure.groups.delete', compact('model'));
    }

    public function postDelete($id)
    {
        $entity = PagesGroup::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->delete();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('groupscontroller.getindex');
        }

        \Flash::success(__('Wpis został usunięty.'));
        return \Redirect::to(url('admin/structure/groups'));
    }
}
