<?php
namespace App\System\Admin\Structure\Controllers;

use App\Models\Core\Website;
use Illuminate\Http\Request;
use App\Extensions\Grid\Grid;
use App\Extensions\Pager\Pager;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\ViewModels\Core\Websites\EditViewModel;
use App\ViewModels\Core\Websites\CreateViewModel;
use App\ViewModels\Core\Websites\DeleteViewModel;

/**
 * Class WebsitesController
 *
 * @package App\System\Admin\Structure\Controllers
 */
class WebsitesController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getIndex()
    {
        $pageId = Input::get('pageId', 1);
        $grid = new Grid();

        $grid->addCol('id', __('ID'));
        $grid->addCol('name', __('Nazwa'), 'edit', ['id']);
        $grid->addCol('symbol', __('Symbol'));
        $grid->addCol('created_at', __('Data utworzenia'));
        $grid->addCol('updated_at', __('Data edycji'));

        $pager = new Pager();
        $pager->setPageIndex($pageId);

        $pager->setRowCount(Website::query()->count());
        $pager->setBaseUrl('admin/structure/websites');

        $websites = Website::query()
            ->skip($pager->getOffset())
            ->take($pager->getPageSize())
            ->get();

        $grid->setPager($pager);
        $grid->addRows($websites);
        $grid->addRowsActionLink('editWebsite', url('admin/structure/websites/edit'), ['id'], __('Edytuj'));
        $grid->addRowsActionLink('deleteWebsite', url('admin/structure/websites/delete'), ['id'], __('Usuń'));

        $grid->addFooterAction('addWebsite', 'link', __('Dodaj'), 'admin/structure/websites/create');

        return view('views.structure.websites.index', ['grid' => $grid]);
    }

    public function getCreate()
    {
        return view('views.structure.websites.create');
    }

    public function postCreate(Request $request)
    {
        $model = new CreateViewModel($request->all());

        $model->is_active = 0;
        if($request->get('is_active'))
        {
            $model->is_active = 1;
        }

        if($model->isValid())
        {
            $model->is_active = empty($model->is_active) ? 0 : $model->is_active;
            $website = new Website();
            $website->fill($model->toArray());
            $website->save();

            \Flash::success(__('Wpis został dodany.'));
            return \Redirect::to(url('admin/structure/websites/edit', [$website->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getEdit($id)
    {
        $entity = Website::find($id);

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('websitescontroller.getindex');
        }

        $model = new EditViewModel($entity);
        $model->fill(Input::old());

        return view('views.structure.websites.edit', compact('model'));
    }

    public function postEdit(Request $request, $id)
    {
        $model = new EditViewModel($request->all());
        if($model->isValid())
        {
            $model->is_active = 0;
            if($request->get('is_active'))
            {
                $model->is_active = 1;
            }

            $entity = Website::find($id);

            if(!$entity)
            {
                \Flash::info(__('Brak wpisu o pdanym ID'));
                return \Redirect::route('websitescontroller.getindex');
            }

            $entity->fill($model->toArray());
            $entity->save();

            \Flash::success(__('Wpis został zapisany.'));
            return \Redirect::to(url('admin/structure/websites/edit', [$entity->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getDelete($id)
    {
        $entity = Website::find($id);

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('websitescontroller.getindex');
        }

        $model = new DeleteViewModel($entity);
        $model->fill(Input::old());

        return view('views.structure.websites.delete', compact('model'));
    }

    public function postDelete($id)
    {
        $entity = Website::where('id', '=', $id)->delete();
        \Flash::success(__('Wpis został usunięty.'));
        return \Redirect::to(url('admin/structure/websites'));
    }
}
