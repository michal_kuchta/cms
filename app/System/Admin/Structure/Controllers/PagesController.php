<?php
namespace App\System\Admin\Structure\Controllers;

use App\Models\Core\Page;
use App\Models\Core\Website;
use Illuminate\Http\Request;
use App\Extensions\Grid\Grid;
use App\Extensions\Pager\Pager;
use App\Models\Core\PagesGroup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\ViewModels\Core\Pages\EditViewModel;
use App\ViewModels\Core\Pages\CreateViewModel;
use App\ViewModels\Core\Pages\DeleteViewModel;

/**
 * Class PagesController
 *
 * @package App\System\Admin\Structure\Controllers
 */
class PagesController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getIndex()
    {
        $pageId = Input::get('pageId', 1);
        $grid = new Grid(self::class . "@index");

        $website = app('app.website');

        $grid->addFilter('id', 'text', null, 'ID', null, null);
        $grid->addFilter('query', 'search', null, __('Szukaj'));
        $grid->addFilter('public', 'checkbox', __('Publiczna'));
        $grid->addFilter('active', 'checkbox', __('Aktywna'));
        $grid->addFilter('module_id', 'select', __(''), null, $website->getModules(true, true, __('Wybierz moduł')));

        $grid->addCol('id', __('ID'));
        $grid->addCol('name', __('Nazwa'));
        $grid->addCol('symbol', __('Symbol'));
        $grid->addCol('created_at', __('Data utworzenia'));
        $grid->addCol('updated_at', __('Data edycji'));

        $pager = new Pager();
        $pager->setPageSize(2);
        $pager->setPageIndex($pageId);

        $pager->setRowCount(Page::query()->count());
        $pager->setBaseUrl('admin/structure/pages');

        $pages = Page::getList($pager, $grid->getFilters());

        $grid->setPager($pager);
        $grid->addRows($pages);
        $grid->addRowsActionLink('editWebsite', url('admin/structure/pages/edit'), ['id'], __('Edytuj'), null, 'btn btn-primary');
        $grid->addRowsActionLink('deleteWebsite', url('admin/structure/pages/delete'), ['id'], __('Usuń'), null, 'btn btn-danger');

        $grid->addFooterAction('addPage', 'link', __('Dodaj'), 'admin/structure/pages/create');

        return view('views.structure.pages.index', ['grid' => $grid]);
    }

    public function postIndex()
    {
        $sessionKey = self::class . "@index";

        $filters = Input::get('filters');
        \Session::put($sessionKey, ['filters' => $filters]);

        if (request()->has('reset'))
        {
            \Session::forget($sessionKey);
        }

        return \Redirect::to(url('admin/structure/pages'));
    }

    public function getCreate()
    {
        $website = app('app.website');
        $groups = PagesGroup::getList($website->id);

        return view('views.structure.pages.create', compact('website', 'groups'));
    }

    public function postCreate(Request $request)
    {
        $model = new CreateViewModel($request->all());
        $model->website_id = app('app.website')->id;
        $model->parent_id = 0;

        if(empty($model->group_id))
            $model->group_id = 0;

        $model->is_active = 0;
        if($request->get('is_active'))
            $model->is_active = 1;

        $model->is_public = 0;
        if($request->get('is_public'))
            $model->is_active = 1;

        if($model->isValid())
        {
            $model->is_active = empty($model->is_active) ? 0 : $model->is_active;
            $page = new Page();
            $page->fill($model->toArray());
            $page->save();

            \Flash::success(__('Wpis został dodany.'));
            return \Redirect::to(url('admin/structure/pages/edit', [$page->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getEdit($id)
    {
        $entity = Page::getPage($id);

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('pagescontroller.getindex');
        }

        $groups = PagesGroup::getList($entity->website_id);

        $model = new EditViewModel($entity);
        $model->fill(Input::old());

        return view('views.structure.pages.edit', compact('model', 'entity', 'groups'));
    }

    public function postEdit(Request $request, $id)
    {
        $model = new EditViewModel($request->all());

        $model->is_active = 0;
        if($request->get('is_active'))
        {
            $model->is_active = 1;
        }

        $model->is_public = 0;
        if($request->get('is_public'))
        {
            $model->is_public = 1;
        }

        if($model->isValid())
        {
            $entity = Page::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();

            if(!$entity)
            {
                \Flash::info(__('Brak wpisu o pdanym ID'));
                return \Redirect::route('pagescontroller.getindex');
            }

            $entity->fill($model->toArray());
            $entity->save();

            \Flash::success(__('Wpis został zapisany.'));
            return \Redirect::to(url('admin/structure/pages/edit', [$entity->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getDelete($id)
    {
        $entity = Page::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('pagescontroller.getindex');
        }

        $model = new DeleteViewModel($entity);
        $model->fill(Input::old());

        return view('views.structure.pages.delete', compact('model'));
    }

    public function postDelete($id)
    {
        Page::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->delete();

        \Flash::success(__('Wpis został usunięty.'));
        return \Redirect::to(url('admin/structure/pages'));
    }
}
