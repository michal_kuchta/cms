<?php
namespace App\System\Admin\Structure\Controllers;

use App\Models\Core\Widget;
use App\Models\Core\Website;
use Illuminate\Http\Request;
use App\Extensions\Grid\Grid;
use App\Extensions\Pager\Pager;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\ViewModels\Core\Widgets\EditViewModel;
use App\ViewModels\Core\Widgets\CreateViewModel;
use App\ViewModels\Core\Widgets\DeleteViewModel;

/**
 * Class WidgetsController
 *
 * @package App\System\Admin\Structure\Controllers
 */
class WidgetsController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getIndex()
    {
        $pageId = Input::get('pageId', 1);
        $grid = new Grid();

        $grid->addCol('id', __('ID'));
        $grid->addCol('name', __('Nazwa'), 'edit', ['id']);
        $grid->addCol('type', __('Typ'));
        $grid->addCol('created_at', __('Data utworzenia'));
        $grid->addCol('updated_at', __('Data edycji'));

        $pager = new Pager();
        $pager->setPageIndex($pageId);

        $query = Widget::query()->where('website_id', '=', app('app.website')->id);

        $pager->setRowCount($query->count());
        $pager->setBaseUrl('admin/structure/widgets');

        $widgets = $query->skip($pager->getOffset())
            ->take($pager->getPageSize())
            ->get();

        $grid->setPager($pager);
        $grid->addRows($widgets);
        $grid->addRowsActionLink('edit', url('admin/structure/widgets/edit'), ['id'], __('Edytuj'));
        $grid->addRowsActionLink('delete', url('admin/structure/widgets/delete'), ['id'], __('Usuń'));

        $grid->addFooterAction('add', 'link', __('Dodaj'), 'admin/structure/widgets/create');

        return view('views.structure.widgets.index', ['grid' => $grid]);
    }

    public function getCreate()
    {
        $website = app('app.website');
        $widgetType = '';

        if(request()->has('action') && request()->get('action', '') != '' && request()->has('type') && request()->get('type', '') != '')
        {
            $widgetConfiguration = 'widgets.cms.'.request()->get('type').'.form';
            $widgetType = request()->get('type');
            if(!view()->exists($widgetConfiguration))
            {
                $widgetConfiguration = '';
            }

            return view('views.structure.widgets.create', compact('website', 'widgetConfiguration', 'widgetType'));
        }

        return view('views.structure.widgets.create', compact('website', 'widgetType'));
    }

    public function postCreate(Request $request)
    {
        $website = app('app.website');
        if(request()->has('action') && request()->get('action', '') != '' && request()->has('type') && request()->get('type', '') != '')
        {
            $widgetConfiguration = 'widgets.cms.'.request()->get('type').'.form';
            $widgetType = request()->get('type');
            if(!view()->exists($widgetConfiguration))
            {
                $widgetConfiguration = '';
            }

            return view('views.structure.widgets.create', compact('website', 'widgetConfiguration', 'widgetType'));
        }

        $model = new CreateViewModel(request()->all());

        $model->is_active = 0;
        if($request->get('is_active'))
        {
            $model->is_active = 1;
        }

        if(request()->has('saveFiles'))
        {
            $files = request()->get('saveFiles', []);

            foreach($files as $key => $file)
            {
                $model->settings[$key] = request()->file("settings[$key]", '')->store('images');
            }
        }

        if($model->isValid())
        {
            $widget = new Widget();
            $widget->fill($model->toArray());
            $widget->save();

            \Flash::success(__('Wpis został dodany.'));
            return \Redirect::to(url('admin/structure/widgets/edit', [$widget->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getEdit($id)
    {
        $website = app('app.website');
        $entity = Widget::query()->where('id', '=', $id)->where('website_id', '=', $website->id)->first();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('widgetscontroller.getindex');
        }

        $model = new EditViewModel($entity);
        $model->fill(Input::old());

        $widgetConfiguration = 'widgets.cms.'.$entity->type.'.form';
        if(!view()->exists($widgetConfiguration))
        {
            $widgetConfiguration = '';
        }

        return view('views.structure.widgets.edit', compact('model', 'widgetConfiguration', 'website'));
    }

    public function postEdit(Request $request, $id)
    {
        $website = app('app.website');

        $model = new EditViewModel($request->all());

        if(request()->has('saveFiles'))
        {
            $files = $request->all()['saveFiles'];
            foreach($files as $key => $file)
            {
                $fileName = $file->getClientOriginalName();
                Storage::disk('local')->putFileAs('public/images/widgets/'.$id, $file, $fileName);
                $model->settings[$key] = 'storage/images/widgets/'.$id.'/'.$fileName;
            }
        }

        $model->is_active = 0;
        if($request->get('is_active'))
        {
            $model->is_active = 1;
        }

        if($model->isValid())
        {
            $entity = Widget::query()->where('id', '=', $id)->where('website_id', '=', $website->id)->first();
            $model->type = $entity->type;

            if(!$entity)
            {
                \Flash::info(__('Brak wpisu o pdanym ID'));
                return \Redirect::route('widgetscontroller.getindex');
            }

            $entity->fill($model->toArray());
            $entity->save();

            \Flash::success(__('Wpis został zapisany.'));
            return \Redirect::to(url('admin/structure/widgets/edit', [$entity->id]));
        }

        return view('views.structure.widgets.edit', compact('model', 'website'));
    }

    public function getDelete($id)
    {
        $entity = Widget::query()->where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('widgetscontroller.getindex');
        }

        $model = new DeleteViewModel($entity);
        $model->fill(Input::old());

        return view('views.structure.widgets.delete', compact('model'));
    }

    public function postDelete($id)
    {
        $entity = Widget::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->delete();
        \Flash::success(__('Wpis został usunięty.'));
        return \Redirect::to(url('admin/structure/widgets'));
    }
}
