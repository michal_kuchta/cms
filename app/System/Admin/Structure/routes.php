<?php

Route::group(['prefix' => config('system.options.admin-prefix')], function()
{
    Route::group(['prefix' => 'structure', 'namespace' => 'App\System\Admin\Structure\Controllers', 'middleware' => ['web', 'auth', 'backend']], function ()
    {
        Route::controller('websites', 'WebsitesController');
        Route::controller('pages', 'PagesController');
        Route::controller('groups', 'GroupsController');
        Route::controller('widgets', 'WidgetsController');
    });
});
