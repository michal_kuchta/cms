<?php

return [
    ['name' => 'structure', 'title' => __('Struktura'), 'url' => 'admin/structure', 'visible' => config('system.options.functionalities.Structure'), 'icon' => 'icon-buffer', 'level' => 2, 'nodes' => [
        ['name' => 'structure', 'title' => __('Witryny'), 'url' => 'admin/structure/websites', 'visible' => true, 'icon' => 'icon-clone', 'level' => 3],
        ['name' => 'pages', 'title' => __('Strony'), 'url' => 'admin/structure/pages', 'visible' => true, 'icon' => 'icon-file', 'level' => 3],
        ['name' => 'groups', 'title' => __('Grupy stron'), 'url' => 'admin/structure/groups', 'visible' => true, 'icon' => 'icon-th-large', 'level' => 3],
        ['name' => 'widgets', 'title' => __('Bloki'), 'url' => 'admin/structure/widgets', 'visible' => true, 'icon' => 'icon-cubes', 'level' => 3]
    ]]
];
