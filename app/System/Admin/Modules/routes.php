<?php

Route::group(['prefix' => config('system.options.admin-prefix')], function()
{
    Route::group(['prefix' => 'modules', 'namespace' => 'App\System\Admin\Modules\Controllers', 'middleware' => ['web', 'auth', 'backend']], function ()
    {
        Route::controller('content', 'ContentController');
        Route::controller('articles', 'ArticlesController');
    });
});
