<?php

return [
    ['name' => 'modules', 'title' => __('Moduły'), 'url' => 'admin/modules', 'visible' => config('system.options.functionalities.Modules'), 'icon' => 'icon-th-list', 'level' => 2, 'nodes' => [
        ['name' => 'content', 'title' => __('Opisowy'), 'url' => 'admin/modules/content', 'visible' => true, 'icon' => 'icon-file-text-o', 'level' => 3],
        ['name' => 'articles', 'title' => __('Artykuły'), 'url' => 'admin/modules/articles', 'visible' => true, 'icon' => 'icon-newspaper-o', 'level' => 3],
    ]]
];
