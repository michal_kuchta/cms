<?php
namespace App\System\Admin\Modules\Controllers;

use App\Models\Core\Page;
use App\Models\Core\Website;
use Illuminate\Http\Request;
use App\Extensions\Grid\Grid;
use App\Models\System\Article;
use App\Extensions\Pager\Pager;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\ViewModels\Modules\Articles\EditViewModel;
use App\ViewModels\Modules\Articles\CreateViewModel;

/**
 * Class ArticlesController
 *
 * @package App\System\Admin\Structure\Controllers
 */
class ArticlesController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getIndex()
    {
        $pageId = Input::get('pageId', 1);
        $grid = new Grid();

        $grid->addCol('id', __('ID'));
        $grid->addCol('name', __('Nazwa'));
        $grid->addCol('symbol', __('Symbol'));
        $grid->addCol('created_at', __('Data utworzenia'));
        $grid->addCol('updated_at', __('Data edycji'));
        $grid->addCol('published_at', __('Data publikacji'));

        $pager = new Pager();
        $pager->setPageIndex($pageId);

        $query = Article::query()->with(['page'])->where('website_id', '=', app('app.website')->id);

        $pager->setRowCount($query->count());
        $pager->setBaseUrl('admin/modules/articles');

        $rows = $query
            ->skip($pager->getOffset())
            ->take($pager->getPageSize())
            ->get();

        $grid->setPager($pager);
        $grid->addRows($rows);
        $grid->addRowsActionLink('editArticle', url('admin/modules/articles/edit'), ['id'], __('Edytuj'));
        $grid->addRowsActionLink('deleteArticle', url('admin/modules/articles/delete'), ['id'], __('Usuń'));

        $grid->addFooterAction('addArticle', 'link', __('Dodaj'), 'admin/modules/articles/create');

        return view('views.modules.articles.index', ['grid' => $grid]);
    }

    public function getCreate()
    {
        $website = app('app.website');
        $pages = Page::getPagesForModule('articles');
        return view('views.modules.articles.create', compact('website', 'pages'));
    }

    public function postCreate(Request $request)
    {
        $model = new CreateViewModel($request->all());

        if($model->isValid())
        {
            $model->is_active = empty($model->is_active) ? 0 : $model->is_active;
            $article = new Article();
            $article->fill($model->toArray());
            $article->save();

            \Flash::success(__('Wpis został dodany.'));
            return \Redirect::to(url('admin/modules/articles/edit', [$article->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getEdit($id)
    {
        $entity = Article::query()
        ->with(['website', 'page'])
        ->where('id', '=', $id)
        ->where('website_id', '=', app('app.website')->id)
        ->firstOrFail();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('articlescontroller.getindex');
        }

        $model = new EditViewModel($entity);
        $model->fill(Input::old());

        $pages = Page::getPagesForModule('articles');

        return view('views.modules.articles.edit', compact('model', 'entity', 'pages'));
    }

    public function postEdit(Request $request, $id)
    {
        $model = new EditViewModel($request->all());
        if($model->isValid())
        {
            $model->is_active = 0;
            if($request->get('is_active'))
            {
                $model->is_active = 1;
            }

            $article = Article::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();
            if(!$article)
            {
                \Flash::info(__('Brak wpisu o pdanym ID'));
                return \Redirect::route('articlescontroller.getindex');
            }
            $article->fill($model->toArray());
            $article->save();

            \Flash::success(__('Wpis został zapisany.'));
            return \Redirect::to(url('admin/modules/articles/edit', [$article->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getDelete($id)
    {
        $entity = Article::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('articlescontroller.getindex');
        }

        $model = new DeleteViewModel($entity);
        $model->fill(Input::old());

        return view('views.modules.articles.delete', compact('model'));
    }

    public function postDelete($id)
    {
        $entity = Article::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->delete();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('articlescontroller.getindex');
        }

        \Flash::success(__('Wpis został usunięty.'));
        return \Redirect::to(url('admin/modules/articles'));
    }
}
