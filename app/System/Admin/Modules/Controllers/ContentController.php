<?php
namespace App\System\Admin\Modules\Controllers;

use App\Models\Core\Page;
use App\Models\Core\Website;
use Illuminate\Http\Request;
use App\Extensions\Grid\Grid;
use App\Models\System\Content;
use App\Extensions\Pager\Pager;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\ViewModels\Modules\Content\EditViewModel;
use App\ViewModels\Modules\Content\CreateViewModel;

/**
 * Class ContentController
 *
 * @package App\System\Admin\Structure\Controllers
 */
class ContentController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getIndex()
    {
        $pageId = Input::get('pageId', 1);
        $grid = new Grid();

        $grid->addCol('id', __('ID'));
        $grid->addCol('title', __('Tytuł'));
        $grid->addCol('symbol', __('Symbol strony'), '', [], 'page');
        $grid->addCol('created_at', __('Data utworzenia'));
        $grid->addCol('updated_at', __('Data edycji'));

        $pager = new Pager();
        $pager->setPageIndex($pageId);

        $query = Content::query()->where('website_id', '=', app('app.website')->id)->with(['page']);

        $pager->setRowCount($query->count());
        $pager->setBaseUrl('admin/modules/content');

        $rows = $query
            ->skip($pager->getOffset())
            ->take($pager->getPageSize())
            ->get();

        $grid->setPager($pager);
        $grid->addRows($rows);
        $grid->addRowsActionLink('editContent', url('admin/modules/content/edit'), ['id'], __('Edytuj'));
        $grid->addRowsActionLink('deleteContent', url('admin/modules/content/delete'), ['id'], __('Usuń'));

        $grid->addFooterAction('addContent', 'link', __('Dodaj'), 'admin/modules/content/create');

        return view('views.modules.content.index', ['grid' => $grid]);
    }

    public function getCreate()
    {
        $website = app('app.website');
        $pages = Page::getPagesForModule('content');
        return view('views.modules.content.create', compact('website', 'pages'));
    }

    public function postCreate(Request $request)
    {
        $model = new CreateViewModel($request->all());

        if($model->isValid())
        {
            $model->is_active = empty($model->is_active) ? 0 : $model->is_active;
            $entity = new Content();
            $entity->fill($model->toArray());
            $entity->save();

            \Flash::success(__('Wpis został dodany.'));
            return \Redirect::to(url('admin/modules/content/edit', [$entity->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getEdit($id)
    {
        $entity = Content::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('contentcontroller.getindex');
        }

        $model = new EditViewModel($entity);
        $model->fill(Input::old());

        $pages = Page::getPagesForModule('content');

        return view('views.modules.content.edit', compact('model', 'entity', 'pages'));
    }

    public function postEdit(Request $request, $id)
    {
        $model = new EditViewModel($request->all());
        if($model->isValid())
        {
            $model->is_active = 0;
            if($request->get('is_active'))
            {
                $model->is_active = 1;
            }

            $entity = Content::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();

            if(!$entity)
            {
                \Flash::info(__('Brak wpisu o pdanym ID'));
                return \Redirect::route('contentcontroller.getindex');
            }

            $entity->fill($model->toArray());
            $entity->save();

            \Flash::success(__('Wpis został zapisany.'));
            return \Redirect::to(url('admin/modules/content/edit', [$entity->id]));
        }
        return \Redirect::refresh()->withInput()->withErrors($model->getErrors());
    }

    public function getDelete($id)
    {
        $entity = Content::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->first();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('contentcontroller.getindex');
        }

        $model = new DeleteViewModel($entity);
        $model->fill(Input::old());

        return view('views.modules.content.delete', compact('model'));
    }

    public function postDelete($id)
    {
        $entity = Content::where('id', '=', $id)->where('website_id', '=', app('app.website')->id)->delete();

        if(!$entity)
        {
            \Flash::info(__('Brak wpisu o pdanym ID'));
            return \Redirect::route('contentcontroller.getindex');
        }

        \Flash::success(__('Wpis został usunięty.'));
        return \Redirect::to(url('admin/modules/content'));
    }
}
