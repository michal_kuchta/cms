<?php

Route::group(['prefix' => config('system.options.admin-prefix'), 'namespace' => 'App\System\Admin\Auth\Controllers', 'middleware' => ['web', 'backend']], function ()
{
    Route::controller('auth', 'AuthController');
});
