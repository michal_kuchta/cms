<?php
namespace App\System\Admin\Auth\Controllers;

use App\Models\Core\User;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\PasswordBroker;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthController extends Controller
{
    /**
     * @var \Illuminate\Auth\AuthManager
     */
    protected $auth;

    /**
     * The password broker implementation.
     *
     * @var PasswordBroker
     */
    protected $passwords;

    /**
     * @var mixed
     */
    protected $loginPath = null;

    /**
     * @var mixed
     */
    protected $redirectTo = null;

    /**
     * Create a new authentication controller instance.
     *
     * @param \Illuminate\Auth\AuthManager $auth
     * @param \Illuminate\Contracts\Auth\PasswordBroker $passwords
     */
    public function __construct(AuthManager $auth, PasswordBroker $passwords)
    {
        $this->auth = $auth;
        $this->passwords = $passwords;

        $this->loginPath = url('admin/auth/login');
        $this->redirectTo = route('home');

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('views.auth.login');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ], [], [
            'username' => __('Nazwa użytkownika'),
            'password' => __('Hasło')
        ]);

        $credentials = $request->only('username', 'password');
        if ($this->auth->guard()->attempt($credentials, $request->has('remember')))
        {
            if (!$this->auth->guard()->user()->isAdmin())
            {
                Auth::logout();

                flash()->error(__('Nie masz udzielonego dostępu do panelu administracyjnego.'));

                return redirect($this->loginPath)->withInput($request->only('email', 'remember'));
            }

            return redirect()->intended($this->redirectTo);
        }

        return redirect($this->loginPath)
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => __('Podany login lub hasło są nieprawidłowe.')
            ]);
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        $this->auth->guard()->logout();
        return redirect('/'.config('system.options.admin-prefix'));
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPassword()
    {
        return view('views.auth.auth.password');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function postPassword(Request $request)
    {
        $model = new RemindViewModel(['email' => $request->get('email')]);

        if ($model->isValid())
        {
            /** @var User $user */
            $user = User::getUserByEmail($model->email);

            if (is_null($user))
            {
                \Flash::error(__('Nie istnieje użytkownik z takim adresem email.'));

                return redirect()->back()->withInput();
            }

            $this->sendLink('emails.password', __('Link do zresetowania hasła'), $user, 'reset');

            \Flash::success(__('Wysłano link do zresetowania hasła'));

            return redirect()->to('admin/auth/login');
        }
        else
        {
            return redirect()->back()->withInput()->withErrors($model->errors());
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * @throws NotFoundHttpException
     * @return \Illuminate\Http\Response
     */
    public function getReset()
    {
        $token = \Request::get('token');
        $tokens = new UserToken();

        if (is_null($tokens->getUserByToken($token, 'reset')))
        {
            \Flash::error(__('Niewłaściwy lub przestarzały token. Wykonaj akcję ponownie.'));

            return redirect()->to('admin/auth/password');
        }

        return view('views.auth.auth.reset')->with('token', $token);
    }

    /**
     * Reset the given user's password.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function postReset(Request $request)
    {
        $model = new ResetViewModel(\Input::all());
        $tokens = new UserToken();

        if ($model->isValid())
        {
            if (User::getUserByEmail($model->email) != $tokens->getUserByToken($model->token, 'reset'))
            {
                return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => __('Niewłaściwy adres E-mail.')]);
            }

            /** @var User $user */
            $user = User::getUserByEmail($model->email);
            $user->password = $model->password;
            $user->save();

            $tokens->deleteToken($model->token);

            \Flash::success(__('Twoje hasło zostało zmienione. Zaloguj się nowymi danymi.'));

            return redirect()->to('admin/auth/login');
        }
        else
        {
            return redirect()->back()->withInput()->withErrors($model->errors());
        }
    }

    /**
     * @param $template
     * @param $subject
     * @param \Edito\Models\Core\User $user
     * @param null $action
     * @param null $value
     */
    private function sendLink($template, $subject, User $user, $action = null, $value = null)
    {
        $tokens = new UserToken();
        $token = $tokens->createToken($user, $action, $value);
        $ip = \Request::getClientIp();
        $username = $user->username;

        \Mail::to($user->email)->queue(new AuthMail($template, $subject, $token, $ip, $username));
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath'))
        {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }

    /**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : '/auth/login';
    }

    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return isset($this->subject) ? $this->subject : __('Link do zresetowania hasła');
    }
}
