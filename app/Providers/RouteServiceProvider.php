<?php

namespace App\Providers;

use Illuminate\Support\Arr;
use Illuminate\Routing\Router;
use App\Extensions\ControllerInspector;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    public function register()
    {
        $this->app->call([$this, 'registerActionsMacors']);
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map(Router $router)
    {
        foreach (config('system.options.functionalities') as $functionality => $enabled)
        {
            if ($enabled)
            {
                if (file_exists(app_path("System/Admin/$functionality/routes.php")))
                {
                    require app_path("System/Admin/$functionality/routes.php");
                }

                if (file_exists(app_path("System/$functionality/routes.php")))
                {
                    require app_path("System/$functionality/routes.php");
                }
            }
        }

        $router->group(['namespace' => $this->namespace], function ($router)
        {
            require app_path('Http/routes.php');
        });
    }

    /**
     * @param Router $router
     */
    public function registerActionsMacors(Router $router)
    {
        /**
         * Route a controller to a URI with wildcard routing.
         *
         * @param  string  $uri
         * @param  string  $controller
         * @param  array   $names
         */
        $router->macro('controller', function ($uri, $controller, $names = [])
        {
            $prepended = $controller;

            if (!empty($this->groupStack))
            {
                $group = end($this->groupStack);

                $prepended = isset($group['namespace']) && strpos($controller, '\\') !== 0 ? $group['namespace'] . '\\' . $controller : $controller;
            }

            $routable = (new ControllerInspector())->getRoutable($prepended, $uri);
            foreach ($routable as $method => $routes)
            {
                foreach ($routes as $route)
                {
                    $action = ['uses' => $controller . '@' . $method];
                    $action['as'] = Arr::get($names, $method);

                    if(empty($action['as']))
                    {
                        $action['as'] = mb_strtolower(str_replace(['\\', '@'], '.', $action['uses']));
                    }

                    $this->{$route['verb']}
                    ($route['uri'], $action);
                }
            }

            /** @var Router $this */
            $this->any($uri . '/{_missing}', $controller . '@missingMethod')->where('_missing', '(.*)');
        });
    }
}
