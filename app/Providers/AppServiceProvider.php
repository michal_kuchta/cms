<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Migrations\MigrationCreator;
use App\Extensions\Database\MigrationCreator as NewMigrationCreator;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->instance(MigrationCreator::class, resolve(NewMigrationCreator::class));

        //Applays custom blade's directives
        require __dir__ . '/../Extensions/Blade/directives.php';
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
