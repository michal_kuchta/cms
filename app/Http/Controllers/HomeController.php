<?php

namespace App\Http\Controllers;

use App\Extensions\Application\Dispatcher;

class HomeController extends Controller
{
    /**
     * @param string|null $path
     * @param string|null $args
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function executeDispatcher($path = null, $args = null)
    {
        $route = \Route::getCurrentRoute();
        $route->forgetParameter('path');
        $route->forgetParameter('args');

        return Dispatcher::route($path, $args);
    }

    public function getHomePage()
    {
        return view('views.home.index');
    }

    /**
     * Catch All
     */
    public function catchAll()
    {
        return view('views.home.index');
    }

    public function catchStorage($path = null)
    {
        Dispatcher::executeStorageDispatcher($path);
    }

}
