<?php

if (config('system.options.frontend'))
{
    Route::group(['middleware' => ['web', 'frontend']], function ()
    {

        Route::any('storage/{path?}', 'HomeController@catchStorage')
        ->where([
            'path' => '[^/\\<>"\'.]+(.*)'
        ]);

        // CMS route
        Route::any('{path}/{args?}', 'HomeController@executeDispatcher')
        ->where([
            'path' => '^(?!admin)[/a-z0-9-]*?',
            'args' => '[^/\\<>"\'.]+(\\.html)?'
        ])
        ->name('frontend');

        // Catch All / 404
        Route::any('{any}', 'HomeController@catchAll')
            ->where('any', '^(?!admin).+')
            ->name('catch');
    });
}
