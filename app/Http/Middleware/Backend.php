<?php
namespace App\Http\Middleware;

use Closure;
use App\Models\Core\Website;
use App\Extensions\Menu\Menu;
use Illuminate\Http\Response;

/**
 * Class Backend
 *
 * @package App\Http\Middleware
 */
class Backend
{
    /**
     * @param \Illuminate\Http\Request $response
     * @param Closure $next
     * @return Response
     */
    public function handle($response, Closure $next)
    {
        $theme = !empty(config('system.options.backend-theme')) ? config('system.options.backend-theme') : 'admin';

        \Event::dispatch('middleware.backend');

        //Copy all functionalities sitemaps to config
        $this->setWebsite();
        $this->setSitemaps();

        //Creates menu
        $menu = new Menu(config('sitemap'));

        //Send menu to view
        \View::share('menu', $menu->get());

        if (!config('system.options.backend', true))
        {
            abort(404);
        }

        if (\Auth::guest())
        {
            \Debugbar::disable();
        }

        \App::bind('path.public', function () use ($theme)
        {
            return base_path() . '/public/themes/' . $theme;
        });

        \View::addLocation(base_path("resources/system/$theme"));

        $response = $next($response);

        return $response;
    }

    public function setSitemaps()
    {
        //Get current sitemap configuration
        $sitemap = config('sitemap');

        foreach (config('system.options.functionalities') as $functionality => $enabled)
        {
            if (file_exists(app_path("System/Admin/$functionality/sitemap.php")))
            {
                //Get sitemaps from functionalities
                $tempSitemap = include app_path("System/Admin/$functionality/sitemap.php");

                if (is_array($tempSitemap) && !empty($tempSitemap))
                {
                    $sitemap = array_merge($sitemap, $tempSitemap);
                }
            }
        }

        //Save new sitemap to configuration
        config(['sitemap' => $sitemap]);
    }

    public function setWebsite()
    {
        $website = null;

        if (!empty($website_id))
        {
            $website = Website::query()->find($website_id);
        }
        else if (isset($_SERVER['SERVER_NAME']) || isset($_SERVER['HTTP_HOST']))
        {
            $baseUrl = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : $_SERVER['HTTP_HOST'];
            $website = Website::query()->where('base_url', '=', $baseUrl)->first();
        }
        if(!$website)
        {
            $website = Website::query()->first();
        }

        app()->singleton('app.website', function() use ($website) { return $website; });
        app()->singleton(Website::class, function() use ($website) { return $website; });
    }
}
