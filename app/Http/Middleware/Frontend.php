<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

/**
 * Class Backend
 *
 * @package App\Http\Middleware
 */
class Frontend
{
    /**
     * @param \Illuminate\Http\Request $response
     * @param Closure $next
     * @return Response
     */
    public function handle($response, Closure $next)
    {
        $theme = !empty(config('system.options.default-theme')) ? config('system.options.default-theme') : 'default';

        \Event::dispatch('middleware.frontend');

        if (!config('system.options.frontend', true))
        {
            abort(404);
        }

        \Debugbar::disable();

        \App::bind('path.public', function () use ($theme)
        {
            return base_path() . '/public/themes/' . $theme;
        });

        \View::addLocation(base_path("resources/system/themes/$theme"));

        $response = $next($response);

        return $response;
    }
}
