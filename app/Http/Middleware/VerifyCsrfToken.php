<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
    ];

    /**
     * @param $request
     * @param Closure $next
     */
    public function handle($request, Closure $next)
    {
        $maxPostSize = intval(stringToByte(ini_get('post_max_size') . 'B'));
        $contentLength = intval(array_get($_SERVER, 'CONTENT_LENGTH', 0));

        try
        {
            if ($contentLength > $maxPostSize)
            {
                \Flash::warning(sprintf(__('Przekroczono maksymalny dozwolony rozmiar wysyłanych danych wynoszący %s.'), byteToString($maxPostSize)));

                return redirect()->refresh();
            }

            return parent::handle($request, $next);
        }
        catch (TokenMismatchException $e)
        {
            \Flash::warning(__('Twoja sesja wygasła. Strona została automatycznie odświeżona aby wygenerować nową sesję.'));

            if ($request->isXmlHttpRequest())
            {
                return response()->json(['error' => ['type' => get_class($e)]], 500);
            }

            return redirect()->refresh()->withInput();
        }
    }

    /**
     * Get the CSRF token from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function getTokenFromRequest($request)
    {
        $token = $request->input('_token') ?: $request->header('X-CSRF-TOKEN');

        if (!$token)
        {
            $token = $request->header('X-XSRF-TOKEN');
        }

        return $token;
    }
}
