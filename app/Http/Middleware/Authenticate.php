<?php
namespace App\Http\Middleware;

use Closure;
use App\Models\Core\User;
use Illuminate\Auth\AuthManager;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class Authenticate
 *
 * @package App\Http\Middleware
 */
class Authenticate
{
    /**
     * The AuthManager implementation.
     *
     * @var AuthManager
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  AuthManager  $auth
     */
    public function __construct(AuthManager $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard($guard)->guest())
        {
            $prefix = config('system.options.admin-prefix');
            $prefix = !empty($prefix) ? $prefix . '/' : '';

            return $this->redirect($request, url($prefix . 'auth/login'));
        }
        else
        {
            /** @var User $user */
            $user = $this->auth->guard($guard)->user();

            if (!$user->is_admin)
            {
                throw new AccessDeniedHttpException();
            }
        }

        return $next($request);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string $url
     * @return mixed
     */
    private function redirect($request, $url)
    {
        if ($request->ajax() || $request->wantsJson())
        {
            return response('Unauthorized.', 401);
        }
        else
        {
            return redirect()->guest($url);
        }
    }
}
