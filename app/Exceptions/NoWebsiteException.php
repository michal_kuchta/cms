<?php
namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class NoWebsiteException extends HttpException
{

}
