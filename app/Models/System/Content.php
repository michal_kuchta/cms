<?php

namespace App\Models\System;

use App\Models\Core\Page;
use App\Models\Core\Website;
use Illuminate\Database\Eloquent\Model;
use App\Extensions\Interfaces\IAuditable;
use App\Extensions\Observers\AuditableObserver;

class Content extends Model implements IAuditable
{
    /**
     * @var string
     */
    protected $table = 'system_content';

    protected $fillable = [
        'id',
        'website_id',
        'page_id',
        'title',
        'content',
        'is_public',
    ];

    /**
     * @var mixed
     */
    public $timestamps = true;

    public static function boot()
    {
        parent::boot();
        parent::observe(new AuditableObserver);
    }

    public function website()
    {
        return $this->hasOne(Website::class, 'id', 'website_id');
    }

    public function page()
    {
        return $this->hasOne(Page::class, 'id', 'page_id');
    }

    public static function getContent($id = null)
    {
        $query = self::query()
            ->with(['website', 'page'])
            ->where('is_public', '=', 1)
            ->where('website_id', '=', app('app.website')->id);

        if ($id)
        {
            $query->where('id', '=', $id);
        }

        return $query
            ->first();
    }

    public static function getContentByPageId($page_id)
    {
        return self::query()
            ->with(['website', 'page'])
            ->where('is_public', '=', 1)
            ->where('page_id', '=', $page_id)
            ->where('website_id', '=', app('app.website')->id)
            ->first();
    }

    public static function getChecboxList()
    {
        $ret = [];
        $content = self::query()
            ->select(['id', 'title'])
            ->where('is_public', '=', 1)
            ->where('website_id', '=', app('app.website')->id)
            ->get();
        
        foreach($content as $r)
        {
            $ret[$r->id] = $r->title;
        }

        return $ret;
    }
}
