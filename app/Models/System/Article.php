<?php

namespace App\Models\System;

use App\Models\Core\Page;
use App\Models\Core\Website;
use App\Extensions\Helpers\File;
use Illuminate\Database\Eloquent\Model;
use App\Extensions\Interfaces\IAuditable;
use App\Extensions\Observers\AuditableObserver;

class Article extends Model implements IAuditable
{
    /**
     * @var string
     */
    protected $table = 'system_articles';

    protected $fillable = [
        'id',
        'website_id',
        'page_id',
        'name',
        'content',
        'is_public',
        'publish_at',
        'unpublish_at',
        'images',
        'show_author',
        'author_type',
        'author_id',
        'author_name',
        'symbol'
    ];

    protected $casts = [
        'images' => 'array'
    ];

    /**
     * @var mixed
     */
    public $timestamps = true;

    public static function boot()
    {
        parent::boot();
        parent::observe(new AuditableObserver);
    }

    public function website()
    {
        return $this->hasOne(Website::class, 'id', 'website_id');
    }

    public function page()
    {
        return $this->hasOne(Page::class, 'id', 'page_id');
    }

    public function images()
    {
        $return = collect();

        if(empty($this->images))
        {
            return $return;
        }

        foreach($this->images as $image)
        {
            $return->push(new File($image));
        }

        return $return;
    }

    public static function getArticle($pageId, $symbol = null)
    {
        $query = self::query()
            ->with(['website', 'page'])
            ->where('is_public', '=', 1)
            ->where('page_id', '=', $pageId)
            ->where('website_id', '=', app('app.website')->id);

            if (!empty($symbol))
            {
                $query->where('symbol', '=', $symbol);
            }

            return $query
            ->first();
    }

    public static function getArticles($pageId)
    {
        $query = self::query()
            ->with(['website', 'page'])
            ->where('is_public', '=', 1)
            ->where('page_id', '=', $pageId)
            ->where('website_id', '=', app('app.website')->id);

            return $query
            ->get();
    }
}
