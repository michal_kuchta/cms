<?php

namespace App\Models\Core;

use App\Models\Core\Website;
use Illuminate\Database\Eloquent\Model;
use App\Extensions\Interfaces\IAuditable;
use App\Extensions\Observers\AuditableObserver;

class PagesGroup extends Model implements IAuditable
{
    protected $table = 'core_page_groups';

    protected $fillable = [
        'id',
        'website_id',
        'name',
    ];

    public static function boot()
    {
        parent::boot();
        parent::observe(new AuditableObserver);
    }

    public function website()
    {
        return $this->hasOne(Website::class, 'id', 'website_id');
    }

    public static function getGroup($id)
    {
        return self::query()
            ->with(['website'])
            ->where('website_id', '=', app('app.website')->id)
            ->find($id);
    }

    public static function getList($website_id)
    {
        $groups = [];
        $groupsO = self::query()->where('website_id', '=', $website_id)->get();
        foreach($groupsO as $group)
        {
            $groups[$group->id] = $group->name;
        }
        return $groups;
    }
}
