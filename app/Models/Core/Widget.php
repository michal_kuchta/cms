<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Interfaces\IAuditable;
use App\Extensions\Interfaces\IActivatable;

class Widget extends Model implements IAuditable
{
    /**
     * @var string
     */
    protected $table = 'core_widgets';

    protected $fillable = [
        'id',
        'website_id',
        'name',
        'type',
        'is_active',
        'settings'
    ];

    protected $casts = [
        'settings' => 'array'
    ];

    /**
     * @var mixed
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $hidden = [];

    public static function boot()
    {
        parent::boot();
    }

    public function website()
    {
        return $this->hasOne(Website::class, 'id', 'website_id');
    }
}
