<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Interfaces\IAuditable;
use App\Extensions\Interfaces\IActivatable;
use App\Extensions\Observers\AuditableObserver;

class Website extends Model implements IAuditable, IActivatable
{
    /**
     * @var string
     */
    protected $table = 'core_websites';

    protected $fillable = [
        'id',
        'name',
        'symbol',
        'theme',
        'is_active',
        'base_url'
    ];

    /**
     * @var mixed
     */
    public $timestamps = true;

    public static function boot()
    {
        parent::boot();
        parent::observe(new AuditableObserver);
    }

    public function isActive()
    {
        return $this->is_active === 1;
    }

    public static function getActive()
    {
        $query = self::query()
            ->where('is_active', '=', 1)
            ->where('website_id', '=', app('app.website')->id);

        $results = $query->get();

        return $results;
    }

    public function getModules($options = false, $addDefault = false, $defaultOptionName = null)
    {
        $modules = [];

        if ($options)
        {
            if ($addDefault && $defaultOptionName)
            {
                $modules['__hiddenField0'] = $defaultOptionName;
            }

            foreach (config('system.modules') as $key => $module)
            {
                $modules[$key] = $module['name'];
            }
        }
        else
        {
            $modules = config('system.modules');
        }

        return $modules;
    }

    public function getWidgets($options = false, $addDefault = false, $defaultOptionName = null)
    {
        $widgets = [];

        if ($options)
        {
            if ($addDefault && $defaultOptionName)
            {
                $widgets['__hiddenField0'] = $defaultOptionName;
            }

            foreach (config('system.widgets') as $key => $module)
            {
                $widgets[$key] = $module['name'];
            }
        }
        else
        {
            $widgets = config('system.modules');
        }

        return $widgets;
    }
}
