<?php

namespace App\Models\Core;

use App\Extensions\Interfaces\IAuditable;
use App\Extensions\Observers\AuditableObserver;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as IAuthenticatable;

class User extends Model implements IAuthenticatable, IAuditable
{
    use Authenticatable;

    /**
     * @var string
     */
    protected $table = 'core_users';

    /**
     * @var mixed
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $guarded = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $attributes = [
        'is_admin' => 0,
        'is_master' => 0,
        'is_active' => 1
    ];

    public static function boot()
    {
        parent::boot();
        parent::observe(AuditableObserver::class);
    }

    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->is_admin;
    }

    public function isMaster()
    {
        return $this->is_master;
    }
}
