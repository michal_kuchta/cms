<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Interfaces\IAuditable;
use App\Extensions\Observers\AuditableObserver;

class Configuration extends Model implements IAuditable
{
    /**
     * @var string
     */
    protected $table = 'core_configuration';

    protected $fillable = [
        'id',
        'website_id',
        'config',
    ];

    protected $casts = [
        'config' => 'array'
    ];

    /**
     * @var mixed
     */
    public $timestamps = true;

    public static function boot()
    {
        parent::boot();
        parent::observe(new AuditableObserver);
    }

    public function website()
    {
        return $this->hasOne(Website::class, 'id', 'website_id');
    }
}
