<?php

namespace App\Models\Core;

use App\Models\Core\Website;
use App\Extensions\Pager\Pager;
use Illuminate\Database\Eloquent\Model;
use App\Extensions\Interfaces\IAuditable;
use App\Extensions\Interfaces\IActivatable;
use App\Extensions\Observers\AuditableObserver;

class Page extends Model implements IAuditable, IActivatable
{

    /**
     * @var string
     */
    protected $table = 'core_pages';

    protected $fillable = [
        'id',
        'website_id',
        'parent_id',
        'name',
        'symbol',
        'ancestors',
        'page_type',
        'slug',
        'is_public',
        'is_active',
        'module_id',
        'group_id'
    ];

    /**
     * @var mixed
     */
    public $timestamps = true;

    public static function boot()
    {
        parent::boot();
        parent::observe(new AuditableObserver);
    }

    public function website()
    {
        return $this->hasOne(Website::class, 'id', 'website_id');
    }

    public function childs()
    {
        return $this->hasMany(Page::class, 'id', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Page::class, 'parent_id', 'id');
    }

    public function isActive()
    {
        return $this->is_active === 1;
    }

    public static function getActive()
    {
        $query = self::query()
            ->where('is_active', '=', 1)
            ->where('website_id', '=', app('app.website')->id);

        $results = $query->get();

        return $results;
    }

    public static function getByPath($website_id, array &$path)
    {
        $path = array_filter($path);

        $page = self::query()
            ->where("website_id", '=', $website_id)
            ->where("is_active", '=', true)
            ->where("symbol", '=', last($path))
            ->first();

        return $page;
    }

    public static function getPage($id)
    {
        return self::query()
            ->with(['website'])
            ->where('website_id', '=', app('app.website')->id)
            ->find($id);
    }

    public static function getPagesForModule($module_id)
    {
        $results = [];

        $res = self::query()
            ->select('id', 'name')
            ->where('module_id', '=', $module_id)
            ->where('is_active', '=', 1)
            ->where('website_id', '=', app('app.website')->id)
            ->get();

        foreach ($res as $row)
        {
            $results[$row->id] = $row['name'];
        }

        return $results;
    }

    public static function getListByGroup($group_id)
    {
        $pages = self::query()
            ->with(['website'])
            ->where('group_id', '=', $group_id)
            ->where('website_id', '=', app('app.website')->id)
            ->get();

        $url = url()->current();
        $parts = explode('/', $url);

        $pages->map(function($item, $key) use ($parts){
            if(preg_match('/'.$item->symbol.'/', last($parts)))
            {
                $item->activePage = true;
            }
            else
            {
                $item->activePage = false;
            }

            return $item;
        });

        return $pages;
    }

    public static function getList(Pager &$pager, $filters)
    {
        $query = self::query();
        $query = self::filters($query, $filters);
        $query->where('website_id', '=', app('app.website')->id);
        $pager->setRowCount($query->count());

        return $query->skip($pager->getOffset())
            ->take($pager->getPageSize())
            ->get();
    }

    private static function filters($query, $filters)
    {
        if(array_has($filters, 'id') && !empty(array_get($filters, 'id')))
        {
            $query->where('id', '=', array_get($filters, 'id'));
        }

        if(array_has($filters, 'query') && !empty(array_get($filters, 'query')))
        {
            $query->where('name', 'LIKE', "%".array_get($filters, 'query')."%");
            $query->where('symbol', 'LIKE', "%".array_get($filters, 'query')."%");
        }

        if(array_has($filters, 'public') && array_get($filters, 'public', 0) > 0)
        {
            $query->where('is_public', '=', array_get($filters, 'public', 0));
        }

        if(array_has($filters, 'active') && array_get($filters, 'active', 0) > 0)
        {
            $query->where('is_active', '=', array_get($filters, 'active', 0));
        }

        if(array_has($filters, 'module_id') && !empty(array_get($filters, 'module_id')))
        {
            $query->where('module_id', '=', array_get($filters, 'module_id'));
        }

        return $query;
    }

    public static function getListForWidget()
    {
        $results = [];

        $res = self::query()
            ->where('website_id', '=', app('app.website')->id)
            ->where('module_id', '=', 'articles')
            ->where('is_active', '=', 1)
            ->get();

        foreach ($res as $row)
        {
            $results[$row->id] = $row->name;
        }

        return $results;
    }

    public static function getPageTypes()
    {
        return [
            0 => __('Standardowa strona'),
            1 => __('Strona główna')
        ];
    }
}
