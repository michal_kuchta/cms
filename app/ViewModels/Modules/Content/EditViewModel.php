<?php
namespace App\ViewModels\Modules\Content;

use App\Extensions\Eloquent\ViewModel;

class EditViewModel extends ViewModel
{
    public $id;
    public $website_id;
    public $page_id;
    public $title;
    public $content;
    public $is_public;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [
            'title' => 'required|min:10|max:128',
            'content' => 'required',
            'page_id' => 'required|exists:core_pages,id',
            'website_id' => 'required|exists:core_websites,id'
        ];
    }
}
