<?php
namespace App\ViewModels\Modules\Content;

use App\Extensions\Eloquent\ViewModel;

class DeleteViewModel extends ViewModel
{
    public $id;
    public $website_id;
    public $page_id;
    public $title;
    public $content;
    public $is_public;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [];
    }
}
