<?php
namespace App\ViewModels\Modules\Articles;

use App\Extensions\Eloquent\ViewModel;

class EditViewModel extends ViewModel
{
    public $website_id;
    public $page_id;
    public $name;
    public $content;
    public $is_public;
    public $images;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [
            'name' => 'required|max:128',
            'content' => 'required',
            'page_id' => 'required|exists:core_pages,id',
            'website_id' => 'required|exists:core_websites,id'
        ];
    }
}
