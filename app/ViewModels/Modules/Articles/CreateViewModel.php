<?php
namespace App\ViewModels\Modules\Articles;

use App\Extensions\Eloquent\ViewModel;

class CreateViewModel extends ViewModel
{
    public $id;
    public $website_id;
    public $page_id;
    public $name;
    public $content;
    public $publish_at;
    public $unpublish_at;
    public $images;
    public $show_author;
    public $author_type;
    public $author_id;
    public $author_name;
    public $symbol;
    public $is_public;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [
            'name' => 'required|max:128',
            'content' => 'required',
            'page_id' => 'required|exists:core_pages,id',
            'website_id' => 'required|exists:core_websites,id',
            'symbol' => 'required|unique:system_articles,symbol',
        ];
    }
}
