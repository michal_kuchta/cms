<?php
namespace App\ViewModels\Core\Users;

use App\Extensions\Eloquent\ViewModel;

class EditViewModel extends ViewModel
{
    public $id;
    public $first_name;
    public $last_name;
    public $email;
    public $new_password;
    public $is_admin;
    public $username;
    public $website_id;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'username' => 'required'
        ];
    }
}
