<?php
namespace App\ViewModels\Core\Users;

use App\Extensions\Eloquent\ViewModel;

class CreateViewModel extends ViewModel
{
    public $username;
    public $first_name;
    public $last_name;
    public $email;
    public $password;
    public $is_admin;
    public $website_id;

    public function __construct($data = null)
    {
        if($data)
            $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'username' => 'required'
        ];
    }
}
