<?php
namespace App\ViewModels\Core\Configuration;

use App\Extensions\Eloquent\ViewModel;

class CreateViewModel extends ViewModel
{
    public $id;
    public $website_id;
    public $config;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [];
    }
}
