<?php
namespace App\ViewModels\Core\PagesGroups;

use App\Extensions\Eloquent\ViewModel;

class CreateViewModel extends ViewModel
{
    public $id;
    public $website_id;
    public $name;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [
            'name' => 'required',
            'website_id' => 'required|exists:core_websites,id'
        ];
    }
}
