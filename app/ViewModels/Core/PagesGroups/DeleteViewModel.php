<?php
namespace App\ViewModels\Core\PagesGroups;

use App\Extensions\Eloquent\ViewModel;

class DeleteViewModel extends ViewModel
{
    public $id;
    public $name;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [];
    }
}
