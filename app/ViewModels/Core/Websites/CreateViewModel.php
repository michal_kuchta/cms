<?php
namespace App\ViewModels\Core\Websites;

use App\Extensions\Eloquent\ViewModel;

class CreateViewModel extends ViewModel
{
    public $id;
    public $name;
    public $symbol;
    public $is_active;
    public $theme;
    public $base_url;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [
            'name' => 'required',
            'theme' => 'required',
            'symbol' => 'required|unique:core_websites,symbol'
        ];
    }
}
