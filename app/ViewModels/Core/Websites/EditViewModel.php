<?php
namespace App\ViewModels\Core\Websites;

use App\Extensions\Eloquent\ViewModel;

class EditViewModel extends ViewModel
{
    public $name;
    public $symbol;
    public $is_active;
    public $theme;
    public $base_url;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [
            'name' => 'required',
            'theme' => 'required',
            'symbol' => 'required'
        ];
    }
}
