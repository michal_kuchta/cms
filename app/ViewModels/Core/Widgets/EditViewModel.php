<?php
namespace App\ViewModels\Core\Widgets;

use App\Extensions\Eloquent\ViewModel;

class EditViewModel extends ViewModel
{
    public $name;
    public $is_active;
    public $settings;
    public $type;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [
            'name' => 'required',
        ];
    }
}
