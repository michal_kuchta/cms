<?php
namespace App\ViewModels\Core\Widgets;

use App\Extensions\Eloquent\ViewModel;

class DeleteViewModel extends ViewModel
{
    public $id;
    public $name;
    public $type;
    public $is_active;
    public $website_id;
    public $settings;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [];
    }
}
