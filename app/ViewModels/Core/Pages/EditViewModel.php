<?php
namespace App\ViewModels\Core\Pages;

use App\Extensions\Eloquent\ViewModel;

class EditViewModel extends ViewModel
{
    public $name;
    public $symbol;
    public $module_id;
    public $is_public;
    public $is_active;
    public $website_id;
    public $group_id;
    public $page_type;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [
            'name' => 'required',
            'module_id' => 'required_if:page_type,0',
            'website_id' => 'required|exists:core_websites,id',
            'group_id' => 'required|min:1'
        ];
    }
}
