<?php
namespace App\ViewModels\Core\Pages;

use App\Extensions\Eloquent\ViewModel;

class CreateViewModel extends ViewModel
{
    public $id;
    public $website_id;
    public $parent_id;
    public $name;
    public $symbol;
    public $ancestors;
    public $page_type;
    public $slug;
    public $is_public;
    public $is_active;
    public $module_id;
    public $group_id;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [
            'name' => 'required',
            'symbol' => 'required|unique:core_pages,symbol',
            'module_id' => 'required_if:page_type,0',
            'website_id' => 'required|exists:core_websites,id',
            'group_id' => 'required|min:1'
        ];
    }
}
