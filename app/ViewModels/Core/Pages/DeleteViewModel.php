<?php
namespace App\ViewModels\Core\Pages;

use App\Extensions\Eloquent\ViewModel;

class DeleteViewModel extends ViewModel
{
    public $id;
    public $name;
    public $symbol;
    public $is_public;
    public $is_active;
    public $module_id;

    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return mixed
     */
    public function getValidatorRules()
    {
        return [];
    }
}
