<?php

namespace Database\Seeds;

use App\Models\Core\Website;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class WebsiteTableSeeder extends Seeder
{
    public function run()
    {
        $website = Website::find(1);

        if ($website == null)
        {
            $website = Website::create([
                'name' => 'default',
                'symbol' => 'default',
                'theme' => 'default',
                'is_active' => 1,
                'created_by' => 1,
                'updated_by' => 1
            ]);
            $website->id = 1;
            $website->save();
        }
    }
}
