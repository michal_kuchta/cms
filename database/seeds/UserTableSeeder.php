<?php

namespace Database\Seeds;

use App\Models\Core\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $user = User::where('username', 'admin')->first();

        if ($user == null)
        {
            $user = User::create([
                'username' => 'admin',
                'password' => Hash::make('admin'),
                'email' => 'admin@admin.pl',
                'first_name' => 'Admin',
                'last_name' => 'Admin',
                'is_admin' => true,
                'is_master' => true,
                'is_active' => true,
                'created_by' => 1,
                'updated_by' => 1
            ]);
            $user->save();
        }
    }
}
