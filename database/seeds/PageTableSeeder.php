<?php

namespace Database\Seeds;

use App\Models\Core\Page;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class PageTableSeeder extends Seeder
{
    public function run()
    {
        $entity = Page::where('page_type', '=', '1')->first();

        if ($entity == null)
        {
            $entity = Page::create([
                'name' => 'Strona główna',
                'symbol' => 'strona-glowna',
                'website_id' => 1,
                'page_type' => 1,
                'is_active' => 1,
                'is_public' => 1,
                'created_by' => 1,
                'updated_by' => 1
            ]);
            $entity->save();
        }
    }
}
