<?php

use Database\Seeds\PageTableSeeder;
use Illuminate\Database\Seeder;
use Database\Seeds\UserTableSeeder;
use Database\Seeds\WebsiteTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(WebsiteTableSeeder::class);
        $this->call(PageTableSeeder::class);
    }
}
