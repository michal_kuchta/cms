<?php

use App\Extensions\Database\Schema;
use Illuminate\Support\Facades\Schema as OldSchema;
use Illuminate\Database\Migrations\Migration;

class AddTableCoreWidgets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Utworzenie schematu bazy
        $schema = new Schema();

        $schema->s->create('core_widgets', function ($table)
        {
            $table->increments('id');
            $table->auditables();
            $table->string('name');
            $table->string('type');
            $table->json('settings')->nullable();
            $table->integer('website_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        OldSchema::dropIfExists('core_widgets');
    }
}
