<?php

use App\Extensions\Database\Schema;
use Illuminate\Support\Facades\Schema as OldSchema;
use Illuminate\Database\Migrations\Migration;

class CreateTableSystemArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Utworzenie schematu bazy
        $schema = new Schema();

        $schema->s->create('system_articles', function ($table)
        {
            $table->increments('id');
            $table->auditables();
            $table->string('name', 128);
            $table->timestamp('publish_at')->nullable();
            $table->timestamp('unpublish_at')->nullable();
            $table->boolean('is_public');
            $table->text('content');
            $table->string('thumb_url', 255)->nullable();
            $table->boolean('show_author')->nullable();
            $table->integer('author_type');
            $table->integer('author_id')->nullable();
            $table->string('author_name', 128);
            $table->integer('website_id');
            $table->integer('page_id');
            $table->string('symbol', 128);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        OldSchema::dropIfExists('system_articles');
    }
}
