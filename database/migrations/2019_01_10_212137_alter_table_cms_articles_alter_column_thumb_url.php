<?php

use App\Extensions\Database\Schema;
use Illuminate\Support\Facades\Schema as OldSchema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCmsArticlesAlterColumnThumbUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Utworzenie schematu bazy
        $schema = new Schema();

        $schema->s->table('system_articles', function ($table)
        {
            $table->dropColumn('thumb_url');
            $table->json('images');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        OldSchema::table('system_articles', function (Blueprint $table)
        {
            $table->string('thumb_url', 255)->nullable();
            $table->dropColumn('images');
        });
    }
}
