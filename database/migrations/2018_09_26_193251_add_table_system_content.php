<?php

use App\Extensions\Database\Schema;
use Illuminate\Support\Facades\Schema as OldSchema;
use Illuminate\Database\Migrations\Migration;

class AddTableSystemContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Utworzenie schematu bazy
        $schema = new Schema();

        $schema->s->create('system_content', function ($table)
        {
            $table->increments('id');
            $table->auditables();
            $table->integer('website_id');
            $table->integer('page_id');
            $table->boolean('is_public');
            $table->string('title', 128);
            $table->text('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        OldSchema::dropIfExists('system_content');
    }
}
