<?php

use App\Extensions\Database\Schema;
use Illuminate\Support\Facades\Schema as OldSchema;
use Illuminate\Database\Migrations\Migration;

class AddTableCorePages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Utworzenie schematu bazy
        $schema = new Schema();

        $schema->s->create('core_pages', function ($table)
        {
            $table->increments('id');
            $table->auditables();
            $table->integer('website_id');
            $table->integer('parent_id')->default(0);
            $table->string('name');
            $table->string('symbol');
            $table->string('ancestors', 512)->nullable();
            $table->integer('page_type')->default(0);
            $table->string('slug', 512)->nullable();
            $table->boolean('is_public')->default(0);
            $table->boolean('is_active')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        OldSchema::dropIfExists('core_pages');
    }
}
