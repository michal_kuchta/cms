<?php

use App\Extensions\Database\Schema;
use Illuminate\Support\Facades\Schema as OldSchema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToWidgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Utworzenie schematu bazy
        $schema = new Schema();

        $schema->s->table('core_widgets', function ($table)
        {
            $table->boolean('is_active')->default(false);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        OldSchema::table('core_widgets', function (Blueprint $table)
        {
            $table->dropColumn('is_active');
        });
    }
}
