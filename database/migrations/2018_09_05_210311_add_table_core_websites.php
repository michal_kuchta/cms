<?php

use App\Extensions\Database\Schema;
use Illuminate\Support\Facades\Schema as OldSchema;
use Illuminate\Database\Migrations\Migration;

class AddTableCoreWebsites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Utworzenie schematu bazy
        $schema = new Schema();

        $schema->s->create('core_websites', function ($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('symbol');
            $table->string('theme');
            $table->boolean('is_active')->default(false);
            $table->auditables();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        OldSchema::dropIfExists('core_websites');
    }
}
