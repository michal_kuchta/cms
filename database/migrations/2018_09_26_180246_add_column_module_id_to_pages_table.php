<?php

use App\Extensions\Database\Schema;
use Illuminate\Support\Facades\Schema as OldSchema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnModuleIdToPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Utworzenie schematu bazy
        $schema = new Schema();

        $schema->s->table('core_pages', function ($table)
        {
            $table->string('module_id', 128);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Utworzenie schematu bazy
        $schema = new Schema();

        OldSchema::table('core_pages', function (Blueprint $table)
        {
            $table->dropColumn('module_id');
        });
    }
}
