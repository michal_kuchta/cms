<h1>Commands description</h1>

<ul>
    <li><b>system:model {name} {--migration}</b> - Tworzenie modelu analogicznie do modelu tworzonego przez make:model z tym, że zapisuje w innym katalogu</li>
    <li><b>system:prepare</b> - Wykonuje odpowiednie komendy aby przygotować cms do pracy z nowymi zmianami</li>
</ul>

<h1>Sass and js compilation:</h1>

 Pod żadnym warunkiem nie wprowadzamy zmian w katalogu <b>public</b>. Wszystkie zmiany związane z edycją styli, szablonów, jsów wykonujemy w katalogu
 <b>resource</b>. Szablon <u>admin</u> panelu administracyjnego znajduje się w <b>resources/system/admin/</b>. Każdy inny szablon powinien znajdować się w katalogu <b>resources/system/themes/{theme}/</b>, gdzie <b>{theme}</b> to nazwa szablonu. W każdym takim szablonie powinien znajdować się plik webpack.mix.js, umożliwający kompilowanie skryptów oraz styli dla danego szablonu.

Zainstaluj node.js z oficjanej strony www. 
Zainstaluj composer z oficjalnej strony www.
Uruchom konsolę CMD lub BASH i przejdź do głównego katalogu projektu, a następnie wykonaj poniższe polecenia w celu pobrania wszystkich wymaganych paczek rozszerzeń: 
 - <b>composer install</b>
 - <b>npm install</p>

Dodatkowo aby móc kompilować pliki sass i js potrzebny będzie pakiet GULP. Można go pobrać poniższą komendą:
<b>npm install -g gulp</b>

Aby skompilować style oraz skrypty dla dowolnego szablonu, należy wywołać poniższą komendę, wstawiąjąc w miejsce <b>{theme}</b> nazwę szablonu:
 - <b>gulp --theme {theme}</b> - pojednyńcza kompilacja
 - <b>gulp watch --theme {theme}</b> - kompilacja styli w czasie rzeczywistym po każdorazowej wprowadzonej zmianie w plikach scss oraz js
 Aby zmimifikować pliki js oraz scss należy dodać flagę <b>--production</b>

 W projekcie jest zaimplementowana obsługa pakietu bower. Aby go używać należy globalnie zainstalować bower poleceniem:
 <b>npm install -g bower</b>
 Aby doinstalować paczkę bowera należy przejść do katalgou szablonu i wywołać polecenie:
 <b>bower install {package_name}</b>
 Następnie ścieżkę do plików tej paczki należy dodać do pliku <b>gulpfile.js</b> znajdującego się w katalogu szablonu.

 Po skompilowaniu plików przy pomocy gulpa, zainstalowana paczka zostanie użyta na stronie.

 Po wykonaniu komendy pliki zostały wygenerowane oraz przeniesione do katalogu zdeklarowanego w pliku webpack.mix.js w wybranym szablonie.

<h1>Nowe funkcje pomocnicze dla Blueprint/Migracji</h1>
<ul>
    <li><b>auditables()</b> - Tworzy pola created_at, updated_at, deleted_at, created_by, updated_by, deleted_by </li>
</ul>

<h1>Obserwatory modeli:</h1>
<ul>
    <li><b>AuditableObserver</b> - Wypełnia pola created_at, updated_at, deleted_at, created_by, updated_by, deleted_by odpowiednimi danymi podczas tworzenia nowego obiektu modelu, podczas zmiany danych obiektu modelu oraz podczas usuwania obiektu modelu </li>
</ul>

<h1>Dodatkowe opcje dla TinyMCE:</h1>
<ul>
    <li><b>hasDiffrentPluginsPath</b> - true/false - pozwala na zmianę domyślnej ścieżki dla pluginów</li>
    <li><b>plugins_url</b> - string - ścieżka do pluginów tinyMCE. Aby zadziałała <b>hasDiffrentPluginsPath</b> musi być ustawione na true</li>
</ul>

<h1>Konfiguracja projektu</h1>
<p>Po pobraniu projektu i wykonaniu "composer install" w głównym katalogu projektu należy utworzyć kopię pliku .env.example i zmienić jego nazwę na .env</p>
<p>Po zmianie nazwy na .env należy zmienić konfigurację wewnątrze tego pliku ustawiając dane do swojej bazy danych</p>
<p>Po poprawnym skonfigurowaniu projektu, należy wykonać poniższe polecenia:</p>
<p><b>php artisan key:generate</b></p>
<p><b>php artisan migrate</b></p>
<p><b>php artisan dump:autoload</b></p>
<p><b>php artisan db:seed</b></p>

<h1>Co zrobić po pobraniu zmian:</h1>
<p><b>composer dump-autoload</b></p>
<p><b>php artisan system:prepare</b></p>

https://stackoverflow.com/questions/36010907/get-tinymce-4-3-image-tools-to-save-images-as-regular-image-files
