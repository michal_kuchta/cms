
var theme = 'default'; // default, admin

var gulp     = require('gulp');
var del      = require('del');
var include  = require('require-dir');
var elixir   = require('laravel-elixir');

var $ = elixir.Plugins;

/* Ustalenie nazwy szablonu */
var themeOption = process.argv.indexOf("--theme");

if (themeOption > -1)
{
    theme = process.argv[themeOption + 1];
}

/* Wczytanie pliku gulpfile.js z katalogu szablonu */
if (theme == 'admin')
{
	include('./resources/system/admin/assets');
}
else
{
	include('./resources/system/themes/'+theme+'/assets');
}

/* Informacja dla użytkownika */
process.stdout.write("\033[1;31m====================================================================\n");
process.stdout.write("    Aktualnie ustawiony szablon to:\033[1;37m "+theme+"\033[1;31m\n\n");
process.stdout.write("    Aby wygenerować pliki dla innego szablonu wywołaj polecenie:\n");
process.stdout.write("        gulp --theme \<nazwa szablonu\>\n");
process.stdout.write("    lub:\n");
process.stdout.write("        gulp watch --theme \<nazwa szablonu\>\n\n");
process.stdout.write("    Aby wygenerować pliki na produkcję wywołaj:\n");
process.stdout.write("        gulp --theme \<nazwa szablonu\> --production\n\n");
process.stdout.write("====================================================================\033[39m\n\n");
