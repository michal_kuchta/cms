{!! Form::group()->select('settings[count]', [1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7, 8=>8, 9=>9, 10=>10])->label(__('Ilość elementów')) !!}
{!! Form::group()->checkbox('settings[is_random]', 1)->label(__('Wyświetl losowo')) !!}
{!! Form::group()->select('settings[pageId]', App\Models\Core\Page::getListForWidget())->label(__('Wybierz stronę z artykułami')) !!}
