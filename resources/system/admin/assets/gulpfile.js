var theme = 'admin';

var elixir = require('laravel-elixir');

var pubdir = "public/themes/"+theme+"/";
var assets = "./resources/system/"+theme+"/assets/";
var bower = assets + "bower/";

elixir.config.sourcemaps = false;
elixir.config.assetsPath = assets;
elixir.config.publicPath = pubdir;


elixir(function(mix)
{
    mix

    .sass([
        assets+"sass/app.scss",
    ], pubdir+"css/")

    .styles([
        bower+"bootstrap/dist/css/bootstrap.css",
        bower+"metisMenu/dist/metisMenu.min.css"
    ], pubdir+"css/vendor.css")

    .scripts([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/foundation-sites/dist/js/foundation.min.js',
        assets+"js/vendor/tinymce/tinymce.js",
        assets+"js/app.js",
        bower+'metisMenu/dist/metisMenu.min.js'
    ], pubdir+'/js/app.js')

    .copy('./node_modules/tinymce/plugins', pubdir+'vendor/tinymce/plugins', false)
    .copy('./node_modules/tinymce/skins', pubdir+'vendor/tinymce/skins', false)
    .copy('./node_modules/tinymce/themes', pubdir+'vendor/tinymce/themes', false)
    .copy(assets+'fonts', pubdir+'fonts', false);
});
