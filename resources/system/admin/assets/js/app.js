
function urlnormalize(string){
    string = string.toLowerCase();
    string = string.replace(' ', '-');
    string = string.replace(',', '-');
    string = string.replace('.', '-');
    string = string.replace(/[^a-z0-9_-]/, '');
    string = string.replace(/[-]+/, '-');
    return string;
}

tinymce.baseUrl = "themes/admin/js/vendor/tinymce/";
var editor_config = {
    path_absolute : "/",
    selector: 'textarea[data-editor]',
    height: 550,
    theme_url: '/themes/admin/vendor/tinymce/themes/modern/theme.js',
    skin_url: '/themes/admin/vendor/tinymce/skins/lightgray',
    plugins_url: '/themes/admin/vendor/tinymce/plugins/',
    hasDiffrentPluginsPath: true,
    plugins: 'print preview fullpage searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
    toolbar1: 'insertfile formatselect | bold italic strikethrough forecolor backcolor | link image media | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
    image_advtab: true,
    templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
    ],
    content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
    ],
    content_style: [
        'body{max-width:700px; padding:30px; margin:auto;font-size:16px;font-family:Lato,"Helvetica Neue",Helvetica,Arial,sans-serif; line-height:1.3; letter-spacing: -0.03em;color:#222} h1,h2,h3,h4,h5,h6 {font-weight:400;margin-top:1.2em} h1 {} h2{} .tiny-table {width:100%; border-collapse: collapse;} .tiny-table td, th {border: 1px solid #555D66; padding:10px; text-align:left;font-size:16px;font-family:Lato,"Helvetica Neue",Helvetica,Arial,sans-serif; line-height:1.6;} .tiny-table th {background-color:#E2E4E7}'
    ],
    visual_table_class: 'tiny-table',
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no"
        });
      }
  };
tinymce.init(editor_config);

jQuery.fn.slug = function(options)
{
    var settings = {
        auto: true, // Automatyczne generowanie sluga podczas pisania
        slug: 'input.slug', // Selector inputa zawierajacego slug
        btn: 'a.clearslug' // Selector przycisku generujacego slug recznie
    };

    if (options)
    {
        jQuery.extend(settings, options);
    }

    var $this = jQuery(this);
    var $dest = jQuery(settings.slug);
    var $btn = jQuery(settings.btn);

    var search = [" ", "ę", "ó", "ą", "ś", "ł", "ż", "ź", "ć", "ń"];
    var replace = ["-", "e", "o", "a", "s", "l", "z", "z", "c", "n"];

    var makeSlug = function(text)
    {
        text = text.toLowerCase();

        var re = /a/;

        for(var i = 0; i < search.length; i++)
        {
            re.compile(search[i], "g");
            text = text.replace(re, replace[i]);
        }

        text = text.replace(/[^a-z0-9-]/g, "");

        if (jQuery.isNumeric($dest.data("ruleMaxlength")))
            text = text.substr(0, $dest.data("ruleMaxlength"));

        return text;
    };

    $this.keyup(function ()
    {
        if (settings.auto)
        {
            var text = makeSlug($this.val());
            var length = Math.min($dest.val().length, text.length);

            if (text.substring(0, length) == $dest.val().substring(0, length))
            {
                $dest.val(text);
            }
        }
    });

    $dest.keydown(function (e)
    {
        if ($.inArray(e.key, search) > -1)
        {
            return false;
        }
    });
    $btn.click(function ()
    {
        $dest.val(makeSlug($this.val()));
    });

    return $this;
};

$(document).ready(function() {
    $('.navbar ul li').on('click', function() {
        index = $(this).index();
        $('.navbar ul li .submenu').removeClass('active');
        $(this).find('.submenu').addClass('active');
    })
});
$(document).ready(function() { // wersja robocza
    $('.navbar ul li:nth-child(1) .submenu .list ul li').on('click', function() {
        $('.addNewWebSite').css('display', 'block');
    })
});

// metisMenu

$(function() {
    $('#menu').metisMenu({
      toggle: false // disable the auto collapse. Default: true.
    });
  });

  (function( $ ){

    $.fn.filemanager = function(type, options) {
        type = type || 'file';

        this.on('click', function(e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

            var target_holder = $('#filemanager_filesBox_' + $(this).data('input'));
            var holderClone = target_holder.find('.filemanager_holder').first().clone(true);
            var name = $(this).data('input');

            window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            window.SetUrl = function (items) {

                var counter = parseInt(target_holder.find('.filemanager_holder').last().attr('data-counter'));

                items.forEach(function (item) {
                    counter = parseInt(counter) + 1;
                    var tempClone = holderClone.clone(true);
                    var target_input = tempClone.find('#' + name);
                    var target_view = tempClone.find('#' + name + '_input');
                    var target_preview = tempClone.find('#' + name + '_holder');

                    if(counter > 0)
                    {
                        target_input.attr('name', name + '[]').attr('id', name + '_' + counter);
                        target_view.attr('id', name + '_input_' + counter);
                        target_preview.attr('id', name + '_holder_' + counter);
                        tempClone.attr('id', tempClone.attr('id') + '_' + counter);
                    }

                    target_input.val('').val(item.url).trigger('change');
                    target_view.val('').val(item.name).trigger('change');
                    target_preview.append(
                        $('<img>').css('height', '5rem').attr('src', item.thumb_url)
                    );
                    target_preview.trigger('change');
                    tempClone.show();

                    tempClone.attr('data-counter', counter);
                    target_holder.append(tempClone);
                });
            };
            return false;
        });
    }
    $('.filemanager_holder').find('.deletePhoto').each(function(){
        $(this).click(function(e){
            e.preventDefault();

            $(this).closest('.filemanager_holder').remove();
        });
    });
  })(jQuery);
