<li @if($node->isActive()) class="mm-active" @endif>
    @if($node->hasNodes())
        <a aria-expanded="true" class="{{ $node->getIcon() }}" href="#"><h1>{{ $node->getTitle() }}</h1></a>
            <ul @if($node->isActive()) class="mm-collapse mm-active" @endif>
                @include('partials.menu.items', ['node' => $node, 'dropdown' => true, 'level' => $level++])
            </ul>
    @else
        <a aria-expanded="false" class="{{ $node->getIcon() }}" href="{{url($node->getUrl())}}">{{ $node->getTitle() }}</a>
    @endif
</li>
{{--
<li>
    @if($node->hasNodes())
        <a class="{{ $node->getIcon() }}" href="#"><h1>{{ $node->getTitle() }}</h1></a>
        <div class="submenu @if($level==0 && $nodeId == 0)active @endif">
            <div class="list">
                <ul>
                    @include('partials.menu.items', ['node' => $node, 'dropdown' => true, 'level' => $level++])
                </ul>
            </div>
        </div>
    @else
        <a class="{{ $node->getIcon() }}" href="{{url($node->getUrl())}}">{{ $node->getTitle() }}</a>
    @endif
</li> --}}

