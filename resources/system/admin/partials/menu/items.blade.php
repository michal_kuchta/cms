@foreach($node->getNodes() as $nodeId => $item)
    @include('partials.menu.item', ['node' => $item, 'dropdown' => $dropdown, 'level' => $level, 'nodeId' => $nodeId])
@endforeach
