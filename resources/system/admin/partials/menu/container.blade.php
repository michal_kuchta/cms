<div style="text-align:center;" class="logo">
    <span style="color:#fff;">logo</span>
</div>
@if($menu->hasNodes())
<nav class="navbar">
    <ul class="matismenu" id="menu">
        @include('partials.menu.items', ['node' => $menu, 'dropdown' => false, 'level' => 0])
        <li>
            <a class="icon-lock" href="/admin/auth/logout"><h1>{{ __('Wyloguj') }}</h1></a>
        </li>
    </ul>
</nav>
@endif
