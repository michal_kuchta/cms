@if (Session::has('flash_notification') && session('flash_notification')->count() > 0)
    @foreach (session('flash_notification') as $flash)
        @if ($flash->overlay)
            @include('partials.alerts.modal', ['modalClass' => 'flash-modal', 'title' => $flash->title, 'body' => $flash->message])
        @else
            <div class="alert position-absolute fixed-top alert-{{ $flash->level }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {!! $flash->message !!}
            </div>
        @endif
    @endforeach

@endif
