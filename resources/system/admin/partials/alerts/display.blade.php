@if (!empty($success))
    <div class="alert position-absolute fixed-top alert-success">
        {{ $success }}
    </div>
@endif

@if (count($errors) > 0)
    <div class="alert position-absolute fixed-top alert-danger">
        <div class="validation-summary-errors">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    </div>
@endif

@include('partials.alerts.flash')
