@if ($filters->count() > 0)
    <div class="filter">
            <h2>Filtry</h2>
        {!! Form::standard()->attr('style', 'width:100%') !!}
        <div class="row">
            @foreach ($filters as $filter)
                <div class="{{ $filter->colClass }}">
                    @switch($filter->type)
                        @case('checkbox')
                            {!! Form::group()->checkbox($filter->name, 1, $filter->default == 1 ? true : false)->label($filter->label) !!}
                            @break
                        @case('text')
                        @case('search')
                            {!! Form::group()->text($filter->name, $filter->default)->label($filter->label)->placeholder($filter->placeholder) !!}
                            @break
                        @case('select')
                            {!! Form::group()->select($filter->name, $filter->values, $filter->default)->label($filter->label)->placeholder($filter->placeholder) !!}
                            @break
                    @endswitch
                </div>
            @endforeach
            <div class="col-2">
                {!! Form::group()
                    ->add(Form::submit('submit', __('Filtruj')))
                    ->add(Form::submit('reset', __('Czyść'))->css('btn-danger')) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endif
