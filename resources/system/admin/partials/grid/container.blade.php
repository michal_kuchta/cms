<div class="websites">
        @include('partials.grid.filters', ['filters' => $filters])
        @include('partials.grid.header', ['columns' => $columns])
        @include('partials.grid.content', ['columns' => $columns, 'rows' => $rows])
        @include('partials.grid.footer', ['actions' => $actions, 'pager' => $pager])
</div>
