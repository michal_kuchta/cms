@if($rows->count() > 0)
    <div class="websites">
        @foreach($rows as $row)
            <div class="row website">
                @foreach($columns as $column)
                    <div class="col">
                        @if(!empty($column->action))
                            <a href="{{ $column->prepareActionLink($row) }}" title="{{ $column->title }}">
                                @include('partials.grid.components.columnName', ['row' => $row, 'column' => $column])
                            </a>
                        @else
                            @include('partials.grid.components.columnName', ['row' => $row, 'column' => $column])
                        @endif
                    </div>
                @endforeach
                @if($rowsActionLinks->count() > 0)
                    <div class="col">
                        @foreach ($rowsActionLinks as $actionLink)
                            <a class="{{ $actionLink->class }}" href="{{ $row->actionLinks->get($actionLink->name) }}">
                                @if ($actionLink->icon)
                                    <i class="{{ $actionLink->icon }}"></i>
                                @else
                                    {{ $actionLink->title }}
                                @endif
                            </a>
                        @endforeach
                    </div>
                @endif
            </div>
        @endforeach
    </div>
@else
    <div class="list">
        <div class="col text-center">
            {{ __('Brak danych do wyświetlenia') }}
        </div>
    </div>
@endif

