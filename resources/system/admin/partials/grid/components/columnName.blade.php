@if (!empty($column->relationName) && method_exists($row, $column->relationName))
        {!! $row->{$column->relationName}->{$column->name} !!}
@else
        {!! $row->{$column->name} !!}
@endif

