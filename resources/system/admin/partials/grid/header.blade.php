<div class="details">
    <div class='row header-table'>
        @foreach($columns as $column)
            <div class="col">{{$column->title}}</div>
        @endforeach
        @if ($showActionsCol)
            <div class="col">{{ __('Akcje') }}</div>
        @endif
    </div>
</div>

