<div class="pagination">
    @if ($pager !== null)
        {!! $pager->render() !!}
    @endif
</div>
<div class="new-subpage">
    @foreach($actions as $action)
        <a class="btn btn-success" href="{{ url($action->url) }}" title="{{ $action->title }}">{{ $action->title }}</a>
    @endforeach
</div>
