@if ($pager->getRowCount() <= $pager->getPageSize())
    <a href="#" class="active">1</a>
@else
    @if ($pager->getTotalPages() > 1 && $pager->getPageIndex() > 1)
        <a href="{{ pagerUrl($pager->getBaseUrl(), ['pageId' => $pager->getPageIndex() - 1]) }}">{{ __('Poprzednia') }}</a>
    @endif
    @for ($i = $pager->getStartPoint(); $i <= $pager->getEndPoint(); $i++)
        @if ($i == $pager->getPageIndex())
            <a href="#" class="active">{{ $pager->getPageIndex() }}</a>
        @else
            <a href="{{ pagerUrl($pager->getBaseUrl(), ['pageId' => $i]) }}">{{ $i }}</a>
        @endif
    @endfor
    @if ($pager->getTotalPages() > $pager->getPageIndex())
        <a href="{{ pagerUrl($pager->getBaseUrl(), ['pageId' => $pager->getPageIndex() + 1]) }}">{{ __('Następna') }}</a>
    @endif
@endif
