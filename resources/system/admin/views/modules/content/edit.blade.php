@extends('layouts.app')

@section('content')
    <div class="col">
        <div class="row">
            <h1>{{ __('Edytuj stronę') }}</h1>
        </div>
        <div class="row">
            {!! Form::standard($model) !!}
            {!! Form::hidden('id') !!}
            {!! Form::hidden('website_id', app('app.website')->id) !!}
            {!! Form::group()->select('page_id', $pages)->label(__('Strona publikacji'))->placeholder(__('Wybierz stronę')) !!}
            {!! Form::group()->text('title')->label(__('Tytuł strony')) !!}
            {!! Form::group()->editor('content')->label(__('Zawartość strony')) !!}
            {!! Form::group()->checkbox('is_public')->label(__('Strona publiczna')) !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Zapisz')))
                ->add(Fluent::link(url('admin/modules/content'), __('Powrót'))) !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('scripts')

@endpush
