@extends('layouts.app')

@section('content')
    <div class="col">
        <div class="row">
            <h1>{{ __('Czy jesteś pewien, że chcesz usunąć tę stronę?') }}</h1>
        </div>
        <div class="content-new-article">
            {!! Form::standard($model) !!}
            {!! Form::hidden('id') !!}
            {!! Form::hidden('website_id', app('app.website')->id) !!}
            {!! Form::group()->select('page_id', $pages)->label(__('Strona publikacji'))->placeholder(__('Wybierz stronę'))->disabled(true) !!}
            {!! Form::group()->text('name')->label(__('Nazwa artykułu'))->disabled(true) !!}
            {!! Form::group()->text('symbol')->label(__('Symbol artykułu'))->disabled(true) !!}
            {!! Form::group()->editor('content')->label(__('Zawartość strony'))->disabled(true) !!}
            {!! Form::group()->datetime('published_at')->label(__('Data publikacji'))->disabled(true) !!}
            {!! Form::group()->datetime('unpublished_at')->label(__('Data usunięcia publikacji'))->disabled(true) !!}
            {!! Form::group()->checkbox('is_public')->label(__('Strona publiczna'))->disabled(true) !!}
            {!! Form::group()->file('thumb')->label(__('Miniaturka zdjęcia'))->disabled(true) !!}
            {!! Form::group()->radios('author_type', [0 => __('Ja'), 1 => __('Podaj ręcznie')])->label(__('Typ autora'))->disabled(true) !!}
            {!! Form::group()->text('author_name')->max(128)->label(__('Autor'))->disabled(true) !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Zapisz')))
                ->add(Fluent::link(url('admin/modules/articles'), __('Powrót'))) !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('scripts')

@endpush
