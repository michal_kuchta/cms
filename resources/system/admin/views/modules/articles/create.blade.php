@extends('layouts.app')

@section('content')
    <div class="new-article">
        <div class="box">
            <h1>{{ __('Dodaj nowy artykuł') }}</h1>
        </div>
        <div class="content-new-article">
            {!! Form::standard() !!}
            {!! Form::hidden('website_id', app('app.website')->id) !!}
            {!! Form::group()->select('page_id', $pages)->label(__('Strona publikacji'))->placeholder(__('Wybierz stronę')) !!}
            {!! Form::group()->text('name')->label(__('Nazwa artykułu')) !!}
            {!! Form::group()->text('symbol')->label(__('Symbol artykułu')) !!}
            {!! Form::group()->editor('content')->label(__('Zawartość strony')) !!}
            {!! Form::group()->datetime('published_at')->label(__('Data publikacji')) !!}
            {!! Form::group()->datetime('unpublished_at')->label(__('Data usunięcia publikacji')) !!}
            {!! Form::group()->checkbox('is_public')->label(__('Strona publiczna')) !!}
            {!! Form::group()->file('thumb')->label(__('Miniaturka zdjęcia')) !!}
            {!! Form::group()->radios('author_type', [0 => __('Ja'), 1 => __('Podaj ręcznie')])->label(__('Typ autora')) !!}
            {!! Form::group()->text('author_name')->max(128)->label(__('Autor')) !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Zapisz')))
                ->add(Fluent::link(url('admin/modules/articles'), __('Powrót'))) !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('scripts')
<script>
    $(document).ready(function(){
        $('input[name="name"]').slug({slug: 'input[name="symbol"]'}).focus();
    });
</script>
@endpush
