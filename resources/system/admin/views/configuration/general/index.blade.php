@extends('layouts.app')

@section('content')
    <div class="new-page">
        <div class="title">
            <h1>{{ __('Konfiguracja - generalna') }}</h1>
        </div>
        <div class="form">
            {!! Form::standard($entity) !!}

            {!! Form::hidden('website_id', app('app.website')->id) !!}
            {!! Form::group()->text('config[name]')->label(__('Nazwa serwisu')) !!}

            {!! Form::group()
                ->add(Form::submit('submit', __('Zapisz'))->css('submitButton')) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
