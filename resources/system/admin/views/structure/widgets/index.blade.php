@extends('layouts.app')

@section('content')
    <h1>{{ __('Zarządzanie blokami') }}</h1>
    {!! $grid->render() !!}
@endsection