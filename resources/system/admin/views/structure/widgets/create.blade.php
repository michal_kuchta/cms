@extends('layouts.app')

@section('content')
    <div class="new-page">
        <div class="title">
            <h1>{{ __('Dodaj nowy blok') }}</h1>
        </div>
        <div class="form">
            {!! Form::standard() !!}

            {!! Fluent::tabs()
                ->add('details', __('Dane podstawowe'))
                ->add('widgets', __('Położenie bloku'))
                ->active('details')
                ->open()
            !!}
                {!! Fluent::tabs()->panel('details') !!}
            {!! Form::hidden('website_id', app('app.website')->id) !!}
                    {!! Form::hidden('action', '') !!}
                    {!! Form::group()->text('name')->label(__('Nazwa bloku')) !!}

                    {!! Form::group()->select2('type', $website->getWidgets(true), $widgetType)->label(__('Typ bloku'))->placeholder(__('Wybierz typ bloku'))->css(['inputType']) !!}
                    {!! Form::group()->checkbox('is_active')->label(__('Blok aktywny')) !!}

                    @if (isset($widgetConfiguration) && !empty($widgetConfiguration))
                        @include($widgetConfiguration, ['model' => null])
                    @endif

                {!! Fluent::tabs()->end() !!}

                {!! Fluent::tabs()->panel('widgets') !!}
                {!! Form::number('settings[section]')->min(0)->max(100)->step(1)->css('sectionField') !!}
                <div>
                    @include('themes.'.$website->symbol)
                </div>
                <script>
                    $(document).ready(function(){
                        $('.changeSection').click(function(e){
                            e.preventDefault();
                            var id = $(this).attr('data-id');
                            $('.sectionField').val(id);
                            $('.changeSection').removeClass('active');
                            $(this).addClass('active');
                        });
                    });
                </script>
                {!! Fluent::tabs()->end() !!}
            {!! Fluent::tabs()->close() !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Zapisz'))->css('submitButton'))
                ->add(Fluent::link(url('admin/structure/widgets'), __('Powrót'))) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(document).ready(function(){
        var form = $('.form > form');
        form.find('input[name="action"]').val('');
        $('.inputType').change(function(){
            form.find('input[name="action"]').val('reload');
            var val = $(this).find('option:selected').val();
            if(val.length == 0)
            {
                form.find('.inputType').val('');
            }
            form.find('.submitButton').trigger('click');
        });
    });
</script>
@endpush
