@extends('layouts.app')

@section('content')
    <div class="new-page">
        <div class="title">
            <h1>{{ __('Czy jesteś pewien, że chcesz usunąć tę stronę?') }}</h1>
        </div>
        <div class="form">
            {!! Form::standard($model) !!}
            {!! Form::hidden('id') !!}
            {!! Form::hidden('website_id', app('app.website')->id) !!}
            {!! Form::group()->text('name')->attr('id', 'page_name')->label(__('Nazwa strony'))->disabled(true) !!}
            {!! Form::group()->text('symbol')->attr('id', 'page_symbol')->label(__('Symbol strony'))->disabled(true) !!}
            {!! Form::group()->checkbox('is_active')->label(__('Strona aktywna'))->disabled(true) !!}
            {!! Form::group()->checkbox('is_public')->label(__('Strona publiczna'))->disabled(true) !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Tak, usuń')))
                ->add(Fluent::link(url('admin/structure/pages'), __('Nie, anuluj'))) !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('scripts')

@endpush
