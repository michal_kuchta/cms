@extends('layouts.app')

@section('content')
    <div class="new-page">
        <div class="title">
            <h1>{{ __('Edytuj stronę') }}</h1>
        </div>
        <div class="form">
            {!! Form::standard($model) !!}
            {!! Form::hidden('id') !!}
            {!! Form::hidden('website_id', app('app.website')->id) !!}
            {!! Form::group()->text('name')->attr('id', 'page_name')->label(__('Nazwa strony')) !!}
            {!! Form::group()->text('symbol')->attr('id', 'page_symbol')->label(__('Symbol strony'))->disabled(true) !!}
            {!! Form::hidden('symbol') !!}
            {!! Form::group()->select('module_id', $entity->website->getModules(true))->label(__('Moduł strony'))->placeholder(__('Wybierz moduł')) !!}
            {!! Form::group()->select('group_id', $groups)->label(__('Grupa stron'))->placeholder(__('Wybierz grupę')) !!}
            {!! Form::group()->checkbox('is_active')->label(__('Strona aktywna')) !!}
            {!! Form::group()->checkbox('is_public')->label(__('Strona publiczna')) !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Zapisz')))
                ->add(Fluent::link(url('admin/structure/pages'), __('Powrót'))) !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('scripts')

@endpush
