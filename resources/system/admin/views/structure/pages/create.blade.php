@extends('layouts.app')

@section('content')
    <div class="new-page">
        <div class="title">
            <h1>{{ __('Dodaj nową stronę') }}</h1>
        </div>
        <div class="form">
            {!! Form::standard() !!}
            {!! Form::hidden('website_id', app('app.website')->id) !!}
            {!! Form::group()->text('name')->label(__('Nazwa strony')) !!}
            {!! Form::group()->text('symbol')->label(__('Symbol strony')) !!}
            {!! Form::group()->select('module_id', $website->getModules(true))->label(__('Moduł strony'))->placeholder(__('Wybierz moduł')) !!}
            {!! Form::group()->select('group_id', $groups)->label(__('Grupa stron'))->placeholder(__('Wybierz grupę')) !!}
            {!! Form::group()->radios('page_type', \App\Models\Core\Page::getPageTypes())->label(__('Typ strony')) !!}
            {!! Form::group()->checkbox('is_active')->label(__('Strona aktywna')) !!}
            {!! Form::group()->checkbox('is_public')->label(__('Strona publiczna')) !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Zapisz')))
                ->add(Fluent::link(url('admin/structure/pages'), __('Powrót'))) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(document).ready(function(){
        $('input[name="name"]').slug({slug: 'input[name="symbol"]'}).focus();
    });
</script>
@endpush
