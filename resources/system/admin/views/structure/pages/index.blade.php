@extends('layouts.app')

@section('content')
    <h1>{{ __('Zarządzanie stronami') }}</h1>
    {!! $grid->render() !!}
@endsection
