@extends('layouts.app')

@section('content')
    <div class="new-page">
        <div class="title">
            <h1>{{ __('Dodaj nową witrynę') }}</h1>
        </div>
        <div class="form">
            {!! Form::standard() !!}
            {!! Form::group()->text('name')->attr('id', 'website_name')->label(__('Nazwa witryny')) !!}
            {!! Form::group()->text('symbol')->attr('id', 'website_symbol')->label(__('Symbol witryny')) !!}
            {!! Form::group()->text('theme')->label(__('Nazwa szablonu')) !!}
            {!! Form::group()->text('base_url')->label(__('Domena witryny')) !!}
            {!! Form::group()->checkbox('is_active')->label(__('Witryna aktywna')) !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Zapisz')))
                ->add(Fluent::link(url('admin/structure/websites'), __('Powrót'))) !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('scripts')
<script>
    $(document).ready(function(){
        $('input[name="name"]').slug({slug: 'input[name="symbol"]'}).focus();
    });
</script>
@endpush
