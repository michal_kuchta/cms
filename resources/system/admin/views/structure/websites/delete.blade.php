@extends('layouts.app')

@section('content')
    <div class="new-page">
        <div class="title">
            <h1>{{ __('Czy jesteś pewien, że chcesz usunąć tę witryne?') }}</h1>
        </div>
        <div class="form">
            {!! Form::standard($model) !!}
            {!! Form::hidden('id') !!}
            {!! Form::group()->text('name')->attr('id', 'website_name')->label(__('Nazwa witryny'))->disabled(true) !!}
            {!! Form::group()->text('symbol')->attr('id', 'website_symbol')->label(__('Symbol witryny'))->disabled(true) !!}
            {!! Form::group()->text('theme')->label(__('Nazwa szablonu'))->disabled(true) !!}
            {!! Form::group()->text('base_url')->label(__('Domena witryny')->disabled(true) !!}
            {!! Form::group()->checkbox('is_active')->label(__('Witryna aktywna'))->disabled(true) !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Tak, usuń')))
                ->add(Fluent::link(url('admin/structure/websites'), __('Nie, anuluj'))) !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('scripts')

@endpush
