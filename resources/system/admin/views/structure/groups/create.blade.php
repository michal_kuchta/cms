@extends('layouts.app')

@section('content')
    <div class="new-page">
        <div class="title">
            <h1>{{ __('Dodaj nową grupę') }}</h1>
        </div>
        <div class="form">
            {!! Form::standard() !!}
            {!! Form::hidden('website_id', app('app.website')->id) !!}
            {!! Form::group()->text('name')->label(__('Nazwa strony')) !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Zapisz')))
                ->add(Fluent::link(url('admin/structure/groups'), __('Powrót'))) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
