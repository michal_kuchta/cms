@extends('layouts.app')

@section('content')
    <div class="new-page">
        <div class="title">
            <h1>{{ __('Czy jesteś pewien, że chcesz usunąć tę grupę?') }}</h1>
        </div>
        <div class="form">
            {!! Form::standard($model) !!}
            {!! Form::hidden('id') !!}
            {!! Form::hidden('website_id', app('app.website')->id) !!}
            {!! Form::group()->text('name')->label(__('Nazwa grupy'))->disabled(true) !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Tak, usuń')))
                ->add(Fluent::link(url('admin/structure/groups'), __('Nie, anuluj'))) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
