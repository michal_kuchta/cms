@extends('layouts.app')

@section('content')
    <h1>{{ __('Zarządzanie grupami') }}</h1>
    {!! $grid->render() !!}
@endsection
