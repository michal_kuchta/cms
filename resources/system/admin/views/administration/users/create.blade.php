@extends('layouts.app')

@section('content')
    <div class="col">
        <div class="row">
            <h1>{{ __('Dodaj użytkownika') }}</h1>
        </div>
        <div class="row">
            {!! Form::standard($model) !!}
            {!! Form::hidden('id') !!}
            {!! Form::hidden('website_id', app('app.website')->id) !!}
            {!! Form::group()->text('username')->label(__('Login')) !!}
            {!! Form::group()->text('first_name')->label(__('Imię')) !!}
            {!! Form::group()->text('last_name')->label(__('Nazwisko')) !!}
            {!! Form::group()->text('email')->label(__('E-mail')) !!}
            {!! Form::group()->password('password')->label(__('Ustaw nowe hasło')) !!}
            @if(Auth::user()->isMaster())
                {!! Form::group()->checkbox('is_admin')->label(__('Uprawnienia administratora')) !!}
            @endif
            {!! Form::group()
                ->add(Form::submit('submit', __('Zapisz')))
                ->add(Fluent::link(url('admin/administration/users'), __('Powrót'))) !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('scripts')

@endpush
