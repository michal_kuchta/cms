@extends('layouts.app')

@section('content')
    <div class="col">
        <div class="row">
            <h1>{{ __('Czy jesteś pewien, że chcesz usunąć wybranego użytkownika?') }}</h1>
        </div>
        <div class="row">
            {!! Form::standard($entity) !!}
            {!! Form::hidden('id') !!}
            {!! Form::hidden('website_id', app('app.website')->id) !!}
            {!! Form::group()->text('username')->label(__('Login'))->disabled(true) !!}
            {!! Form::group()->text('first_name')->label(__('Imię'))->disabled(true) !!}
            {!! Form::group()->text('last_name')->label(__('Nazwisko'))->disabled(true) !!}
            {!! Form::group()->text('email')->label(__('E-mail'))->disabled(true) !!}
            {!! Form::group()
                ->add(Form::submit('submit', __('Tak')))
                ->add(Fluent::link(url('admin/administration/users'), __('Nie'))) !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('scripts')

@endpush
