@extends('layouts.login')

@section('content')
    <header>
        <h1>
            {{ __('Zaloguj się') }}
        </h1>
    </header>
    <div class="logowanie">
        <form method="POST" action="" aria-label="{{ __('Login') }}">
            @include('partials.alerts.display', ['errors' => $errors])
            @csrf
            <input id="username" type="username" placeholder="Username or email" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
            @if ($errors->has('username'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
            <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
           <div class="remember">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                <label class="form-check-label" for="remember">
                    {{ __('Zapamiętaj mnie') }}
                </label>
           </div>
            <button type="submit" class="btn btn-primary">
                {{ __('Zaloguj') }}
            </button>
        </form>
    </div>
    <div class="author">
        <h2>Rozwiązań dostarcza: <a href="#">nazwa-firmy.pl</a></h2>
    </div>
@endsection
