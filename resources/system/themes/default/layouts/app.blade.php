<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('/themes/'.$themeName.'/js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('/themes/'.$themeName.'/css/app.css') }}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/themes/{{ $themeName }}/vendor/tinymce/skins/lightgray/content.min.css">
</head>
<body>
    <div id="app">
        <div class="row">
            <div class="col-2">
                    @include('partials.widget-container', ['index' => '1'])
            </div>
            <div class="col-2">
                    @include('partials.widget-container', ['index' => '2'])
            </div>
            <div class="col-2">
                    @include('partials.widget-container', ['index' => '9'])
            </div>
            <div class="col-2">
                    @include('partials.widget-container', ['index' => '10'])
            </div>
            <div class="col-2">
                    @include('partials.widget-container', ['index' => '11'])
            </div>
            <div class="col-2">
                    @include('partials.widget-container', ['index' => '12'])
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                    @include('partials.widget-container', ['index' => '14'])
            </div>
            <div class="col-6">
                    @include('partials.widget-container', ['index' => '15'])
            </div>
        </div>
        <div class="row">
            @include('partials.widget-container', ['index' => '3'])
        </div>
        <div class="row">
            <div class="col-2">
                    @include('partials.widget-container', ['index' => '4'])
            </div>
            <div class="col">
                <main class="py-4">
                    @yield('content')
                </main>
            </div>
            <div class="col-2">
                @include('partials.widget-container', ['index' => '5'])
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                    @include('partials.widget-container', ['index' => '13'])
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                    @include('partials.widget-container', ['index' => '6'])
            </div>
            <div class="col-6">
                    @include('partials.widget-container', ['index' => '7'])
            </div>
        </div>
        <footer>
            @include('partials.widget-container', ['index' => '8'])
        </footer>
    </div>
</body>
</html>
