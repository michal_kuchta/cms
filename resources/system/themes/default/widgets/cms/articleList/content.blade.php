@if(!empty($settings['articlesModels']) && count($settings['articlesModels']) > 0)
    @foreach ($settings['articlesModels'] as $item)
        <div>
            <div>{!! $item->name !!}</div>
            <div>{!! $item->content !!}</div>
        </div>
    @endforeach
@endif
