@if(!empty($settings['contentsModels']) && count($settings['contentsModels']) > 0)
    @foreach ($settings['contentsModels'] as $item)
        <div>
            <div>{!! $item->title !!}</div>
            <div>{!! $item->content !!}</div>
        </div>
    @endforeach
@endif
