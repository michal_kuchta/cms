<ul style="display: inline;">
    @foreach ($menuItems as $item)
        <li style="display: inline;padding: 10px;" @if($item->activePage) class="active" @endif>
            <a href="{{ url($item->symbol) }}">{{ $item->name }}</a>
        </li>
    @endforeach
</ul>
