var theme = 'default';

var elixir = require('laravel-elixir');

var pubdir = "public/themes/"+theme+"/";
var assets = "resources/system/themes/"+theme+"/assets/";

elixir.config.sourcemaps = false;
elixir.config.assetsPath = assets;
elixir.config.publicPath = pubdir;


elixir(function(mix)
{
    mix

    .sass(assets+"sass/app.scss", pubdir+"css/")

    .scripts([
        './node_modules/jquery/dist/jquery.min.js',
        assets+"js/app.js"
    ], pubdir+'/js/app.js')

    .copy('./node_modules/tinymce/skins', pubdir+'/vendor/tinymce/skins', false);
});
