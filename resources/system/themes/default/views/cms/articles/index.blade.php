@extends('layouts.app')

@section('content')
    <h1>{{ $pageTitle }}</h1>
    <div class="content">
        @if (!empty($articles))
            @foreach ($articles as $article)
                <div class="row">
                    <div class="col-3">
                        <span>kiedyś zdjecie tu bedzie</span>
                    </div>
                    <div class="col">
                        <div class="row"><h2><a href="{{ url(app('app.page')->symbol, [$article->symbol]) }}">{{ $article->name }}</a></h2></div>
                        <div class="row">{!! $article->content !!}</div>
                    </div>
                </div>
            @endforeach
        @else
            <h2>Brak artykułów</h2>
        @endif
    </div>
@endsection
