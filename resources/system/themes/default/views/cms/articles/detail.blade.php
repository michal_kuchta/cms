@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col">
        <h1>{{ $article->name }}</h1>
        <div class="content">
            {!! $article->content !!}
        </div>
        <div class="row">{{ __('Autor') }}: {{ $article->author_name }}</div>
        <div class="row">{{ __('Data publikacji') }}: {{ $article->published_at }}</div>
        @if (!empty($article->images))
            <div class="images">
                @foreach ($article->images() as $item)
                    <a href="{{ $item->getFileUrl() }}">
                        <img src="{{ $item->getThumbUrl() }}">
                    </a>
                @endforeach
            </div>
        @endif
    </div>
</div>

@endsection
