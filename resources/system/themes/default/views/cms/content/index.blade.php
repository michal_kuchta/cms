@extends('layouts.app')

@section('content')
    @if ($content == null)
        <h1>{{ __('Brak tresci do wyświetlenia') }}</h1>
    @else
        <h1>{{ $content->title }}</h1>
        <div class="content">
            {!! $content->content !!}
        </div>
    @endif
@endsection
